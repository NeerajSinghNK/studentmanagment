@extends('layouts.student')

@section('title', 'Dashboard')
@section('css')
<style>
    .countdown {
      text-transform: uppercase;
      font-weight: bold;
    }

    .countdown span {
      text-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
      font-size: 3rem;
      margin-left: 0.8rem;
    }

    .countdown span:first-of-type {
      margin-left: 0;
    }

    .countdown-circles {
      text-transform: uppercase;
      font-weight: bold;
    }

    .countdown-circles span {
      width: 100px;
      height: 100px;
      border-radius: 50%;
      background: rgba(255, 255, 255, 0.2);
      display: flex;
      align-items: center;
      justify-content: center;
      box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.1);
    }

    .countdown-circles span:first-of-type {
      margin-left: 0;
    }

    body {
      min-height: 100vh;
    }

    .bg-gradient-1 {
      background: #7f7fd5;
      background: -webkit-linear-gradient(to right, #7f7fd5, #86a8e7, #91eae4);
      background: linear-gradient(to right, #7f7fd5, #86a8e7, #91eae4);
    }

    .bg-gradient-2 {
      background: #654ea3;
      background: -webkit-linear-gradient(to right, #654ea3, #eaafc8);
      background: linear-gradient(to right, #654ea3, #eaafc8);
    }

    .bg-gradient-3 {
      background: #ff416c;
      background: -webkit-linear-gradient(to right, #ff416c, #ff4b2b);
      background: linear-gradient(to right, #ff416c, #ff4b2b);
    }

    .bg-gradient-4 {
      background: #007991;
      background: -webkit-linear-gradient(to right, #007991, #78ffd6);
      background: linear-gradient(to right, #007991, #78ffd6);
    }

    .rounded {
      border-radius: 1rem !important;
    }

    .btn-demo {
      padding: 0.5rem 2rem !important;
      border-radius: 30rem !important;
      background: rgba(255, 255, 255, 0.3);
      color: #fff;
      text-transform: uppercase;
      font-weight: bold !important;
    }

    .btn-demo:hover,
    .btn-demo:focus {
      color: #fff;
      background: rgba(255, 255, 255, 0.5);
    }


    .button {
      transition-duration: 0.4s;
    }

    .button:hover {
      background-color: #4CAF50;
      /* Green */
      border-radius: 12px;
      color: rgba(255, 0, 0, 0.3);
    }
  </style>
@stop
@section('content_header')
<div class="container">
  <div class="row mb-2">
    <div class="col-sm-6">
      <h1 class="m-0 text-dark"> Zoom Class Links <small>...</small></h1>
    </div><!-- /.col -->
    <div class="col-sm-6">
      <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{url('student/dashboard')}}">Home</a></li>
        <li class="breadcrumb-item active">Class Links</li>
        <!-- <li class="breadcrumb-item active">Top Navigation</li> -->
      </ol>
    </div><!-- /.col -->
  </div><!-- /.row -->
</div><!-- /.container-fluid -->
@stop
@section('content')

<div class="container">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">

      <!-- Link all -->
      <div class="card card-primary card-outline">
        <div class="card-header">
          <h3 class="card-title">Class Links</h3>

          <div class="card-tools">
            <div class="input-group input-group-sm">
              <!-- <input type="text" class="form-control" placeholder="Search Mail">
                  <div class="input-group-append">
                    <div class="btn btn-primary">
                      <i class="fas fa-search"></i>
                    </div>
                  </div> -->
            </div>
          </div>
          <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
          <div class="table-responsive mailbox-messages">
            <table class="table table-hover table-striped">
              <tbody>
                @if($link)
                  @if ($link->start_at < date('Y-m-d H:i:s') && $link->end_at > date('Y-m-d H:i:s'))
                    <tr>
                      <td class="mailbox-subject">
                          <button class="btn btn-success" type="button" onclick="event.preventDefault(); document.getElementById('postLink').submit();">
                              Go to class
                          </button>
                        <form id="postLink" action="{{url('student/postLink')}}" method="POST">
                          <input type="hidden" id="postLink" name="postLink" value="{{$link->link_id}}">
                          @csrf
                        </form>                             
                      </td>
                      <td class="mailbox-attachment"></td>
                    </tr>
                  @else
                      <div class="container py-5">
                        <p class="mb-4 font-weight-bold text-uppercase">Class Start in</p>
                        <div class="py-5">
                          <div class="row">
                            <div class="col-lg-8 mx-auto">
                              <div class="rounded bg-gradient-1 text-white shadow p-5 text-center mb-5">
                                <p class="mb-4 font-weight-bold text-uppercase">Class Start in</p>
                                <div id="clock-b" class="countdown-circles d-flex flex-wrap justify-content-center pt-4">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  @endif
                @else
                  <tr>
                    <td>NO LINk FOUND</td>
                  </tr>
                @endif
              </tbody>
            </table>
            <!-- /.table -->
          </div>
          <!-- /.mail-box-messages -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer p-0">
          <div class="mailbox-controls">
            <!-- Check all button -->
            <!-- <button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="far fa-square"></i>
            </button>
            <div class="btn-group">
              <button type="button" class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fas fa-reply"></i></button>
              <button type="button" class="btn btn-default btn-sm"><i class="fas fa-share"></i></button>
            </div> -->
            <!-- /.btn-group -->
            <!-- <button type="button" class="btn btn-default btn-sm"><i class="fas fa-sync-alt"></i></button>
            <div class="float-right">
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-left"></i></button>
                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-chevron-right"></i></button>
              </div>
            </div> -->
            <!-- /.float-right -->
          </div>
        </div>
      </div>
      <!-- /.card -->


    </div>
    <!-- /.col-md-6 -->
  </div>
  <!-- /.row -->
</div><!-- /.container-fluid -->
@stop

@section('plugins.viewLink', true)
@section('js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.countdown/2.2.0/jquery.countdown.min.js"></script>

<script type="text/javascript">
  var startTime = "{{$link ? $link->start_at:""}}";  
  console.log(startTime);
  $(function () {
     $('#clock-b').countdown(startTime).on('update.countdown', function(event) {
      var $this = $(this).html(event.strftime(''
        + '<span class="h1 font-weight-bold">%D</span> Day%!d'
        + '<span class="h1 font-weight-bold">%H</span> Hr'
        + '<span class="h1 font-weight-bold">%M</span> Min'
        + '<span class="h1 font-weight-bold">%S</span> Sec'));
    });

  });

</script>

@stop