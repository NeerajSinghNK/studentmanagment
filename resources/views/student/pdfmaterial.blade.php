@extends('layouts.student')

@section('title', 'Dashboard')

@section('content_header')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('student/dashboard')}}">Home</a></li>
              <li class="breadcrumb-item active">PDF Material</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div><!-- /.container-fluid -->
@stop
@section('content')


<!-- Main content -->
<section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          @if(getLinkCountByType('Excersise',$subject->id,$chapter->id) > 0)
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
              <h3>{{getLinkCountByType('Excersise',$subject->id,$chapter->id)}}<sup style="font-size: 20px">Excerise</sup></h3>

                <p>Excersise</p>
              </div>
              <div class="icon">
              <i class="fas fa-file-pdf"></i>
              </div>
              <a href="{{url('student/excersiseMaterial')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          @endif
          <!-- ./col -->
          @if(getLinkCountByType('Theory',$subject->id,$chapter->id) > 0)
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <h3>{{getLinkCountByType('Theory',$subject->id,$chapter->id)}}<sup style="font-size: 20px">Theory</sup></h3>

                <p>Theory</p>
              </div>
              <div class="icon">
              <i class="fas fa-file-pdf"></i>
              </div>
              <a href="{{url('student/theoryMaterial')}}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          @endif
          <!-- ./col -->
         
          <!-- ./col -->
        </div>
</section>
@stop

@section('css')

@stop

@section('js')


@stop