@php

$user = auth()->user();
@endphp
<form role="form" action="{{url('/student/saveProfile')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
 
  <div class="card-body">
    <div class="form-group @if($errors->has('first_name')) has-error @endif">
      @php
      
      $value = old('first_name',$user->first_name);
      @endphp
      <div class="form-group">
        <label for="inputFname">Firstname</label>
        <input type="text" class="form-control" name="first_name" id="inputFname" value='{{$value}}'
          placeholder="Enter Firstname">
      </div>
      @if ($errors->has('first_name'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('first_name') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('last_name')) has-error @endif">
      @php
      $value = old('last_name',$user->last_name);
      @endphp
      <div class="form-group">
        <label for="inputLname">Lastname</label>
        <input type="text" class="form-control" name="last_name" id="inputLname" value='{{$value}}'
          placeholder="Enter Lastname">
      </div>
      @if ($errors->has('last_name'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('last_name') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('email')) has-error @endif">
      @php
      $value = old('email',$user->email);
      @endphp
      <div class="form-group">
        <label for="inputEmail1">Email</label>
        <input type="email" class="form-control" name="email" id="inputEmail1" value='{{$value}}'
          placeholder="Enter email">
      </div>
      @if ($errors->has('email'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('email') }}</strong>
      </p>
      @endif
    </div>
   
      

    

    <div class="form-group  @if($errors->has('password')) has-error @endif">
      <div class="form-group">
        <label for="inputPassword">Password</label>
        <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Enter Password" value="">
      </div>
      @if ($errors->has('password'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('password') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group  @if($errors->has('password_confirmation')) has-error @endif">
      <div class="form-group">
        <label for="inputconpassword">Confirm Password</label>
        <input type="password" class="form-control" name="password_confirmation" id="inputconpassword"
          placeholder="Confirm Password">
      </div>
      @if ($errors->has('password_confirmation'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('password_confirmation') }}</strong>
      </p>
      @endif
    </div>
  </div>
                
  <div class="form-group  @if($errors->has('image')) has-error @endif">
      <div class="input-group">
        <div class="custom-file col-md-7">
          <input type="hidden" name="profile_pic" value="{{$user->image}}">
          <input type="file" name="image" value="{{$user->image}}" class="custom-file-input ">
          <lable id="pic" class="custom-file-label" for="customFile">Choose Profile Picture</lable>
        </div>
      </div>
      @if ($errors->has('image'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('image') }}</strong>
      </p>
      @endif
    </div>

  <!-- /.card-body -->
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>


