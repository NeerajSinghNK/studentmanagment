@extends('adminlte::page')

@section('title', 'IIT-PULSE')

@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Teacher</li>
        </ul>
      </div>
@stop

@section('content')
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
          <button type="button" class="btn btn-success select-email" data-toggle="modal" data-target="#modal-default">
            Select All Email
          </button>
          <a href="{{url('admin/teacher/add')}}"><button type="button" class="btn btn-success">Add teacher</button></a> 
          <div class="col-md-12">
            <!-- @foreach($teachers as $teach)
            <a href="{{url('admin/teacher/teach/'.$teach->id)}}"><button type="button" class="btn btn-success">
              {{$teach->teach_name}}
            </button></a> 
            @endforeach -->
          </div>
              <table id="userlist" style="width: 100%" class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($teachers  as $reskey => $teacher)
                  <tr>
                    <td>{{$reskey+1}}</td>
                    <td>{{$teacher->user->first_name??""}} </td>
                    <td>{{$teacher->user->last_name??""}} </td>
                    <td class="email">{{$teacher->user->email}} </td>
                    <td><a href="{{url('admin/teacher/update/'.$teacher->id)}}" class="edit"> 
                      <span class="oi" data-glyph="pencil"></span>
                      <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                      </i></a> &nbsp;&nbsp;

                      <a href="{{url('admin/teacher/delete/'.$teacher->id)}}" data-id="{{$teacher->id}}" class="delete-row"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"></i></a></td>
                  </tr>
                @endforeach
                </tbody>
              </table>
        </div>
    </div>
    <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Default Modal</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">  
                  <textarea class="form-control" id='all-email'></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button id="copy-email" type="button" class="btn btn-primary">Copy</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush
@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)
@section('js')
<script> 
  var table = $('#userlist').dataTable({"scrollX": true});
  console.log(table);
  const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

  
  $('.select-email').click(function() {
      var email = table.api().columns(3).data().eq( 0 ).join(','); // Reduce the 2D array into a 1D array of data ;
      $('#all-email').val(email);
  });

  $('#copy-email').click(function() {
      $('#all-email').select();
      document.execCommand('copy');
      Toast.fire({
        type: 'success',
        title: 'All email copied'
      });
      $("#modal-default .close").click();
  });
</script>
@stop
