@extends('adminlte::page')

@section('title', 'IIT-PULSE')

@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Subject</li>
        </ul>
      </div>
@stop

@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
<!--
          <button type="button" class="btn btn-success select-Category" data-toggle="modal" data-target="#modal-default">
            Select Subject Category
-->
          </button>
          <a href="{{url('admin/subject/add')}}"><button type="button" class="btn btn-success">Add subject</button></a> 
          <div class="col-md-12">
           
          </div>
              <table id="userlist" style="width: 100%" class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Subject Catagory</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($subjects  as $reskey => $subject)
                  <tr>
                    <td>{{$reskey+1}}</td>
                    <td>{{$subject->name??""}} </td>
                    <td>{{$subject->subject_category??""}} </td>              

                    <td><a href="{{url('admin/subject/update/'.$subject->id)}}" class="edit"> 
                      <span class="oi" data-glyph="pencil"></span>
                      <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                      </i></a> &nbsp;&nbsp;
                      <a onclick="return confirm('Are you sure, you want to detete?')" href="{{url('admin/subject/delete/'.$subject->id)}}" data-id="{{$subject->id}}" class="delete-row"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"></i></a></td>
                  </tr>
                @endforeach 
                </tbody>
              </table>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush
@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)
@section('js')
<script> 
  var table = $('#userlist').dataTable({"scrollX": true});
  
</script>
@stop