@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<!-- <h1>Dashboard</h1> -->
<div class="app-title">
  <div>
    <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
  </div>
  <ul class="app-breadcrumb breadcrumb">
  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Subject</li>
  </ul>
</div>
@stop

@section('content')
<!-- <p>Welcome to this beautiful admin panel.</p> -->

<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Subject</h3>
              </div>

            <div class="tab-pane" id="settings">

              <form role="form" action="{{url('/admin/subject/save')}}" method="post">

              <form role="form" action="{{url('admin/subject/save')}}" method="post">

                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $subject->id }}">
<!--                <input type="hidden" name="id" value="{{ $subject->id }}">  -->
                <div class="card-body">
                  <div class="form-group @if($errors->has('name')) has-error @endif">
                    
                    <div class="form-group">
                      <label for="inputFname">Name</label>
                      <input type="text" class="form-control" name="name" id="inputFname" value='{{old('name',$subject->name)}}'
                        placeholder="Enter name">
                    </div>
                    @if ($errors->has('name'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('name') }}</strong>
                    </p>
                    @endif
                  </div>

                  <div class="form-group @if($errors->has('subject_category')) has-error @endif">
                   
                    <div class="form-group">
                      <label for="inputLname">Category</label>
                      <input type="text" class="form-control" name="subject_category" id="inputLname" value='{{ old('subject_category',$subject->subject_category)}}'
                        placeholder="Enter Subject category">
                    </div>
                    @if ($errors->has('subject_category'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('subject_category') }}</strong>
                    </p>
                    @endif
                  </div>

<!--
                 
-->
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
              <!-- <form class="form-horizontal">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form> -->
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush

@section('js')
<script> console.log('Hi!'); </script>
@stop 