@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<!-- <h1>Dashboard</h1> -->
<div class="app-title">
  <div>
    <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
  </div>
  <ul class="app-breadcrumb breadcrumb">
  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Chapter</li>
  </ul>

  @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
  
</div>
@stop

@section('content')
<!-- <p>Welcome to this beautiful admin panel.</p> -->
 
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Chapter</h3>
              </div>

            <div class="tab-pane" id="settings">
              <form role="form" action="{{url('admin/chapter/save')}}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $chapter->id }}">
                <div class="card-body">
                  <div class="form-group @if($errors->has('chap_name')) has-error @endif">
                    
                    <div class="form-group">
                      <label for="inputFname">Chapter Name</label>
                      <input type="text" class="form-control" name="chap_name" id="inputFname" value='{{old('chap_name',$chapter->chap_name)}}'
                        placeholder="Enter Chapter name">
                    </div>
                    @if ($errors->has('chap_name'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('chap_name') }}</strong>
                    </p>
                    @endif
                  </div>

                  <div class="form-group @if($errors->has('chap_type')) has-error @endif">
                    @php 
                      $value = old('chap_type',$chapter->chap_type); 
                    @endphp
                    <div class="form-group">
                      <label for="inputchap_type1">chapter Type</label>            
                        <select class="custom-select" name="chap_type">
                          <option @if($value == "type 1") selected="selected" @endif value="type 1">type 1</option>
                          <option @if($value == "type 2") selected="selected" @endif value="type 2">type 2</option>
                          <option @if($value == "type 3") selected="selected" @endif value="type 3">type 3</option>
                          <option @if($value == "type 4") selected="selected" @endif value="type 4">type 4</option>
                          <option @if($value == "type 5") selected="selected" @endif value="type 5">type 5</option>
                        </select>
                    </div>
                    @if ($errors->has('chap_type'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('chap_type') }}</strong>
                    </p>
                    @endif
                  </div>

                  <div class="form-group @if($errors->has('chap_class')) has-error @endif">
                    @php
                      $class = explode(",",$chapter->chap_class);
                      $value = old('chap_class',$class);
                    @endphp
                    <div class="form-group">
                      <label for="inputclass1">Class</label>
                      <select class="custom-select select2" name="chap_class[]" multiple>
                        <option 
                        @if(in_array("class 9th",$value)) selected="selected" @endif value="class 9th">
                          class 9th
                        </option>
                        <option 
                        @if(in_array("class 10th",$value)) selected="selected" @endif value="class 10th">
                          class 10th
                        </option>
                        <option 
                        @if(in_array("class 11th",$value)) selected="selected" @endif value="class 11th">
                          class 11th
                        </option>
                        <option 
                        @if(in_array("class 12th",$value)) selected="selected" @endif value="class 12th">
                          class 12th
                        </option>
                        <option 
                        @if(in_array("Dropper",$value)) selected="selected" @endif value="Dropper">
                          Dropper
                        </option>
                      </select>
                    </div>
                    @if ($errors->has('chap_class'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('chap_class') }}</strong>
                    </p>
                    @endif
                  </div>

                  <div class="form-group @if($errors->has('priority')) has-error @endif">
                    <div class="form-group">
                      <label for="inputLname">priority</label>
                      <input type="number" class="form-control" name="priority" id="inputLname" value="{{ old('priority',$chapter->priority)}}"
                        placeholder="Enter priority" step="0.1" min="1">
                    </div>
                    @if ($errors->has('priority'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('priority') }}</strong>
                    </p>
                    @endif
                  </div>
                  <div class="form-group @if($errors->has('subject_id')) has-error @endif">
                    @php
                      $value=old('subject_id',$chapter->subject_id);
                    @endphp
                    <div class="form-group">
                      <label for="inputFname">Subject</label>
                      <select  class="form-control" name="subject_id">
                        @foreach($subjects as $subject)
                           <option @if($value == $subject->id) selected="selected" @endif value="{{$subject->id}}">
                               {{$subject->name}}</option>
                          @endforeach
                        </select> 
                    </div>
                    @if ($errors->has('subject_id'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('subject_id') }}</strong>
                    </p>
                    @endif
                  </div>
                  <div class="form-group @if($errors->has('subject_id')) has-error @endif">
                      @php 
                       $value = old('subject_id',$chapter->subject_id); 
                      @endphp
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush
@section('plugins.multiselect', true)

@section('js')
<script> 
  $('.select2').select2({theme: 'bootstrap4'}); 
  </script>
@stop 