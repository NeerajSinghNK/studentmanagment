@extends('adminlte::page')

@section('title', 'IIT-PULSE')
@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      #wrapper {
        margin: auto;
        padding:0 10px;
        box-sizing: border-box;
        text-align: center;
      }
    </style>
@endpush
@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Chapter</li>
        </ul>
      </div>
@stop

@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
          <!-- <button type="button" class="btn btn-success select-email" data-toggle="modal" data-target="#modal-default">
            Add Subject Categary
          </button> -->
          <div id="wrapper">
            <div class="row">
                <div class="col-md-3 mb-4">
                    <select class="select2" id="subject">   
                      <option value=''>--Select Subject--</option>
                      <option value="{{url('admin/chapter/all')}}">all</option>
                      @php $subjectId = $subjectId ?? '' @endphp
                      @foreach($subjects as $subject)
                        <option 
                          @if($subjectId == $subject->id) selected='selected' @endif 
                          value="{{url('admin/chapter/subject/'.$subject->id)}}">
                          {{$subject->name}}
                        </option>
                      @endforeach
                    </select>
                </div>
                <!-- <div class="col-md-3 mb-4">
                    <select class="form-control select2_demo_2 select2-hidden-accessible" name="filter_subject_id" id="filter_subject_id" tabindex="-1" aria-hidden="true">
                        <option value="">--Select Subject--</option>
                        <option value="10">Mathemetics</option><option value="11">Science</option><option value="12">Social Science</option>                    </select><span class="select2 select2-container select2-container--bootstrap4" dir="ltr" style="width: 209px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter_subject_id-container"><span class="select2-selection__rendered" id="select2-filter_subject_id-container" title="--Select Subject--">--Select Subject--</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="form-control select2_demo_2 select2-hidden-accessible" name="filter_sub_subject_id" id="filter_sub_subject_id" tabindex="-1" aria-hidden="true">
                        <option value="">--Select sub subject--</option>
                                            </select><span class="select2 select2-container select2-container--bootstrap4" dir="ltr" style="width: 209px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter_sub_subject_id-container"><span class="select2-selection__rendered" id="select2-filter_sub_subject_id-container" title="--Select sub subject--">--Select sub subject--</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="form-control select2_demo_2 select2-hidden-accessible" name="filter_chapter_id" id="filter_chapter_id" tabindex="-1" aria-hidden="true">
                        <option value="">--Select Chapter--</option>
                                            </select><span class="select2 select2-container select2-container--bootstrap4" dir="ltr" style="width: 209px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter_chapter_id-container"><span class="select2-selection__rendered" id="select2-filter_chapter_id-container" title="--Select Chapter--">--Select Chapter--</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="form-control select2_demo_2 select2-hidden-accessible" name="filter_membership_type" id="filter_membership_type" tabindex="-1" aria-hidden="true">
                        <option value="">--Membership--</option>
                        <option value="free">Free</option>
                        <option value="paid">Paid</option> 
                    </select><span class="select2 select2-container select2-container--bootstrap4" dir="ltr" style="width: 209px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter_membership_type-container"><span class="select2-selection__rendered" id="select2-filter_membership_type-container" title="--Membership--">--Membership--</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="form-control select2_demo_2 select2-hidden-accessible" name="filter_status" id="filter_status" tabindex="-1" aria-hidden="true">
                        <option value="">--Select Status--</option>
                        <option value="1">Active</option>
                        <option value="0">Deactive</option> 
                    </select><span class="select2 select2-container select2-container--bootstrap4" dir="ltr" style="width: 209px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-filter_status-container"><span class="select2-selection__rendered" id="select2-filter_status-container" title="--Select Status--">--Select Status--</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                </div> -->
                <div class="col-md-1">
                  <button id="search" type="button" class="btn btn-primary">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
            </div>
            <!-- <div class="col-md-2">  
              
            </div> -->
          </div>
          <a href="{{url('admin/chapter/add')}}"><button type="button" class="btn btn-success">Add chapter</button></a> 
          <div class="col-md-12">
           
          </div>
              <table id="userlist" style="width: 100%" class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Chapter Name</th>
                    <th>Chapter Type</th>
                    <th>Priority</th>
                    <th>Class</th>
                    <th>Subject</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($chapters  as $reskey => $chapter)
                  <tr>
                    <td>{{$reskey+1}}</td>
                    <td>{{$chapter->chap_name}} </td>
                    <td>{{$chapter->chap_type}} </td>
                    <td>{{$chapter->priority}} </td>
                    <td>{{$chapter->chap_class}} </td>
                    <td>{{$chapter->subject->name}} </td>
                    <td><a href="{{url('admin/chapter/update/'.$chapter->id)}}" class="edit"> 
                      <span class="oi" data-glyph="pencil"></span>
                      <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                      </i></a> &nbsp;&nbsp;
                      <a onclick="return confirm('Are you sure,you want to delete? ')" href="{{url('admin/chapter/delete/'.$chapter->id)}}" data-id="{{$chapter->id}}" class="delete-row"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"></i></a></td>
                  </tr>
                @endforeach 
                </tbody>
              </table>
        </div>
    </div>
    <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Default Modal</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">  
                  <textarea class="form-control" id='all-email'></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button id="copy-email" type="button" class="btn btn-primary">Copy</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@stop


@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)
@section('plugins.multiselect', true)
@section('js')
<script> 
  var table = $('#userlist').dataTable({"scrollX": true});
   $('.select2').select2({
      theme: 'bootstrap4'
   });

  $("#search").click(function () {
    var subject = $('#subject').val()
    if(subject != null && subject != ""){
      window.location.href = subject; 
    }
  });
</script>
@stop
