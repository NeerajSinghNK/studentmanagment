@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<!-- <h1>Dashboard</h1> -->
<div class="app-title">
  <div>
    <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active"><a href="{{url('admin/link/all')}}">Link</a></li>
    <li class="breadcrumb-item active">@if($link->id) Update @else Add @endif Link</li>
  </ul>
</div>
@stop

@section('content')
<!-- <p>Welcome to this beautiful admin panel.</p>  -->

<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">@if($link->id) Update @else Add @endif Link</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form id="link-form" role="form" action="{{url('admin/link/save')}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="{{ $link->id }}">
          <div class="card-body">
            <div class="form-group @if($errors->has('title')) has-error @endif">
              @php $value = old('title',$link->title); @endphp
              <div class="form-group">
                <label for="inputTitle">Link Title</label>
                <input type="text" class="form-control" name="title" placeholder="Title" value="{{$value}}">
              </div>
              @if ($errors->has('title'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('title') }}</strong>
              </p>
              @endif
            </div>
            <div class="form-group @if($errors->has('description')) has-error @endif">
              @php
              $value = old('description',$link->description);

              @endphp
              <div class="form-group">
                <label for="inputTitle">Link Description</label>

                <textarea id="description" name="description" rows="4" cols="50"
                  placeholder="Write some text...">{{$value}}</textarea>
              </div>
              @if ($errors->has('description'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('description') }}</strong>
              </p>
              @endif
            </div>
            <div class="form-group @if($errors->has('link')) has-error @endif">
              @php
              $value = old('link',$link->link);
              @endphp
              <div class="form-group">
                <label for="inputFname">Create Link</label>
                <input type="text" class="form-control" name="link" placeholder="Paste Link Here" value="{{$value}}">
              </div>
              @if ($errors->has('link'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('link') }}</strong>
              </p>
              @endif
            </div>

            <div class="form-group @if($errors->has('batch_id')) has-error @endif">
              @php $value = old('batch_id',$link->batch->pluck('id')->toArray());
              @endphp
              <div class="form-group">
                <label for="inputFname">Batch</label>
                <select class="select2" multiple data-placeholder="Select a Batch" name="batch_id[]">
                  @foreach($batchs as $batch)
                  <option @if(in_array($batch->id,$value)) selected="selected" @endif value="{{$batch->id}}">
                    {{$batch->batch_name}}</option>
                  @endforeach
                </select>
              </div>
              @if ($errors->has('batch_id'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('batch_id') }}</strong>
              </p>
              @endif
            </div>

            <div class="form-group @if($errors->has('subject_id')) has-error @endif">
              @php
              $value = old('subject_id',$link->subject_id);
              @endphp
              <div class="form-group">
                <label for="inputclass1">Subject</label>
                <!-- <select class="select2 select2-hidden-accessible" name="batch_id[]" multiple="" data-placeholder="Select a Batch" data-dropdown-css-class="select2-purple" style="width: 100%;" data-select2-id="15" tabindex="-1" aria-hidden="true"> -->
                <select class="custom-select select2" name="subject_id">
                  <option selected="">Select</option>
                  @foreach($subjects as $subject)
                  <option @if($value==$subject->id) selected="selected" @endif value="{{$subject->id}}">
                    {{$subject->name}}
                  </option>
                  @endforeach
                </select>
              </div>
              @if ($errors->has('subject_id'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('subject_id') }}</strong>
              </p>
              @endif
            </div>

            <div class="form-group @if($errors->has('chapter')) has-error @endif">
              @php
              $chapter = old('chapter',$link->chapter_id);
              @endphp
              <div class="form-group">
                <label for="inputTitle">Chapter</label>
                <select name="chapter_id" class="form-control select2">
                </select>
              </div>
              @if ($errors->has('chapter'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('chapter') }}</strong>
              </p>
              @endif
            </div>


            <div class="form-group @if($errors->has('type')) has-error @endif">
              @php
              $value = old('type',$link->type);
              @endphp

              <div class="form-group">
                <label for="inputclass1">Link Type</label>
                <select class="custom-select" name="type">
                  <option selected>Select Please</option>
                  <option @if($value=="Class" ) selected="selected" @endif value="Class">Class</option>
                  <option @if($value=="Theory" ) selected="selected" @endif value="Theory">Theory</option>
                  <option @if($value=="Previous" ) selected="selected" @endif value="Previous">Previous</option>
                  <option @if($value=="Excersise" ) selected="selected" @endif value="Excersise">Excersise</option>
                  <option @if($value=="Other" ) selected="selected" @endif value="Other">Other</option>

                </select>
              </div>
              @if ($errors->has('type'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('type') }}</strong>
              </p>
              @endif
            </div>


            <div class="form-group @if($errors->has('reservationtime')) has-error @endif">
              @php
              $start_at = date('m/d/Y h:i A',strtotime($link->start_at));
              $end_at = date('m/d/Y h:i A',strtotime($link->end_at));
              $reservationtime = $link->start_at ? $start_at." - ".$end_at : "";
              $value = old('reservationtime',$reservationtime);
              @endphp
              <div class="form-group">
                <label for="inputFname">Date and Time Range:</label>
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                  </div>
                  <input type="text" class="form-control float-right" id="reservationtime" name="reservationtime"
                    required readonly value="{{$value}}">
                </div>
                <!-- /.input group -->
              </div>
              @if ($errors->has('reservationtime'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('reservationtime') }}</strong>
              </p>
              @endif
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
    <div class="col-md-6">
      <div class="card previous-upload-card collapsed-card">
              <div class="card-header" data-card-widget="collapse">
                <h3 class="card-title">Previous Upload</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <!-- /.card-body -->
              <div class="card-footer bg-white p-0">
                <ul class="nav nav-pills flex-column" id="previous-upload">
                  <!-- <li class="nav-item">
                    <a href="#" class="nav-link">
                      United States of America
                      <span class="float-right text-danger">
                        <i class="fas fa-arrow-down text-sm"></i>
                        12%</span>
                    </a>
                  </li> -->
                </ul>
              </div>
              <!-- /.footer -->
      </div>
      <div class="card instruction collapsed-card">
              <div class="card-header" data-card-widget="collapse">
                <h3 class="card-title">Instruction</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="chart-responsive"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                      <canvas id="pieChart" height="92" width="185" class="chartjs-render-monitor" style="display: block; width: 185px; height: 92px;"></canvas>
                    </div>
                    <!-- ./chart-responsive -->
                  </div>
                  <!-- /.col -->
                  <div class="col-md-4">
                    <ul class="chart-legend clearfix">
                      <li><i class="far fa-circle text-danger"></i> Chrome</li>
                      <li><i class="far fa-circle text-success"></i> IE</li>
                      <li><i class="far fa-circle text-warning"></i> FireFox</li>
                      <li><i class="far fa-circle text-info"></i> Safari</li>
                      <li><i class="far fa-circle text-primary"></i> Opera</li>
                      <li><i class="far fa-circle text-secondary"></i> Navigator</li>
                    </ul>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.card-body -->
              <div class="card-footer bg-white p-0">
                <ul class="nav nav-pills flex-column">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      United States of America
                      <span class="float-right text-danger">
                        <i class="fas fa-arrow-down text-sm"></i>
                        12%</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      India
                      <span class="float-right text-success">
                        <i class="fas fa-arrow-up text-sm"></i> 4%
                      </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      China
                      <span class="float-right text-warning">
                        <i class="fas fa-arrow-left text-sm"></i> 0%
                      </span>
                    </a>
                  </li>
                </ul>
              </div>
              <!-- /.footer -->
      </div>
    </div>
</div>
@stop

  @push('css')
  <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
  @endpush


  @section('plugins.dateTimePicker', true)
  @section('plugins.multiselect', true)
  @section('js')

  <script type="text/javascript">
    var BaseUrl = "{{url('')}}"
    $(document).ready(function () {
      $('select[name="subject_id"]').trigger('change');
    });

    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    });
    $('.select2').select2({
      theme: 'bootstrap4'
    });
    $('select[name="subject_id"]').on('change', function () {
      var subjectID = $(this).val();
      if (subjectID) {
        $.ajax({
          url: BaseUrl + '/admin/link/dynamicChapter/' + subjectID,
          type: "GET",
          dataType: "html",
          success: function (data) {
            var chapterId = "{{$chapter}}";
            $('select[name="chapter_id"]').html(data);
            $('select[name="chapter_id"] option[value="' + chapterId + '"]').attr('selected', 'selected');
            $('select[name="chapter_id"]').select2({ theme: 'bootstrap4' })

          }
        });
      }
      else {
        alert("Something went wrong....");
      }
    });

    $('select').on('change', function () {
      var linkform = $('#link-form').serialize();
      
        $.ajax({
          url: BaseUrl + '/admin/link/getpreviouslink',
          type: "POST",
          // dataType: "html",
          data    : linkform,
          success: function (response) {
              // console.log(response);
              $('#previous-upload').html(response);
          }
        });
    });



  </script>
  @stop