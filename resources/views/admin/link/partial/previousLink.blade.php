@if($links->count() > 0)
	@foreach($links as $link)
		<li class="nav-item col-md-12">
			<a href="{{url('admin/link/update/'.$link->id)}}" class="nav-link">
				{{$link->title}}
				<span class="float-right ">
				<i class="fas fa-arrow-down text-sm"></i>
				</span>
			</a>
		</li>
	@endforeach
@endif

                       