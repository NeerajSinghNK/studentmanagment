@extends('adminlte::page')

@section('title', 'IIT-PULSE')

@push('css')

  <style type="text/css">
    table{
      table-layout: fixed; 
      word-wrap:break-word !important;
    }
  </style>
@endpush

@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Link</li>
        </ul>
      </div>
@stop


@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
          <form action="{{url('admin/link/all')}}" method="POST">
          @csrf
          <div id="wrapper">
            <div class="row">
                <div class="col-md-3 mb-4">
                    <select class="select2" name="subjects[]" multiple placeholder='Subjects'>   
                      <!-- <option value="">all</option> -->
                      @foreach(\App\Models\Subject::all() as $subject)
                        <option 
                          @if(in_array($subject->id,$search['subjects'])) selected='selected' 
                          @endif 
                          value="{{$subject->id}}">
                          {{$subject->name}}
                        </option>
                      @endforeach
                    </select>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="select2" name="chapters[]" multiple placeholder='Chapters'>   
                      <!-- <option value="">all</option> -->
                      @foreach(\App\Models\Chapter::all() as $Chapter)
                        <option 
                          @if(in_array($Chapter->id,$search['chapters'])) selected='selected' @endif 
                          value="{{$Chapter->id}}">
                          {{$Chapter->chap_name}}
                        </option>
                      @endforeach
                    </select>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="select2" name="batchs[]" multiple placeholder='Batchs'>   
                      <!-- <option value="">all</option> -->
                      @foreach(\App\Models\Batch::all() as $Batch)
                        <option 
                          @if(in_array($Batch->id,$search['batchs'])) selected='selected' @endif 
                          value="{{$Batch->id}}">
                          {{$Batch->batch_name}}
                        </option>
                      @endforeach
                    </select>
                </div>
                <div class="col-md-3 mb-4">
                    <select class="select2" name="types[]" multiple placeholder='Types'>   
                        <!-- <option value="">all</option> -->
                        <option @if(in_array('Class',$search['types'])) selected='selected' @endif value="Class">Class</option>
                        <option @if(in_array('Theory',$search['types'])) selected='selected' @endif value="Theory">Theory</option>
                        <option @if(in_array('Previous',$search['types'])) selected='selected' @endif value="Previous">Previous</option>
                        <option @if(in_array('Excersise',$search['types'])) selected='selected' @endif value="Excersise">Excersise</option>
                        <option @if(in_array('Other',$search['types'])) selected='selected' @endif value="Other">Other</option>
                    </select>
                </div>
                <div class="col-md-1">
                  <button type="submit" class="btn btn-primary">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
            </div>
            <!-- <div class="col-md-2">  
              
            </div> -->
          </div>
          </form>
          <a href="{{url('admin/link/add')}}"><button type="button" class="btn btn-success">Add link</button></a> 
          
                <table id="userlist" style="width: 100%" class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th>Action</th>
                      <th style="width: 10px">#</th>
                      <th>Title</th>
                      <th>Links</th>
                      <!-- <th>Description</th> -->
                      <th>Batch</th>
                      <!-- <th>Subject</th>
                      <th>Chapter</th>
                      <th>Types</th> -->
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Added By</th>

                    </tr>
                  </thead>
                  <tbody>
                  @foreach($links  as $reskey => $link)
                    <tr>
                      <td width="12%">
                        <a href="{{url('admin/link/update/'.$link->id)}}" class="edit"> 
                          <span class="oi" data-glyph="pencil"></span>
                          <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="Edit" data-original-title="Edit"></i>
                        </a>
                        &nbsp;
                        <a href="{{url('admin/link/delete/'.$link->id)}}" data-id="{{$link->id}}" class="delete-row">
                          <i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"> 
                          </i>
                        </a>
                        &nbsp;
                        <a href="{{$link->link}}" data-id="{{$link->id}}" target="_blank">
                          <i class="fas fa-external-link-alt" aria-hidden="true" data-toggle="tooltip" title="redirect to url" data-original-title="redirect to url"> 
                          </i>
                        </a>
                        &nbsp;
                        <i class="fa fa-copy" aria-hidden="true" data-toggle="tooltip" title="copy url" data-original-title="copy url" data-url="{{$link->link}}" style="color: #007bff;"> 
                        </i>
                      </td>
                      <td>{{$reskey+1}}</td>
                      <td>{{$link->title}}</td>
                      <td data-toggle="tooltip" title="{{$link->link}}" width="15%">
                        @php $redirecturl = str_replace('www.','',str_replace("https://","",$link->link)); @endphp
                        {{substr($redirecturl, 0, 10)}}...
                      </td>
                      <!-- <td></td> -->
                      <td width="300px">
                        @if($link->batch->count() > 0)
                          @foreach($link->batch as $key => $batch)
                          {{$batch->batch_name}}
                          @if($key+1 != $link->batch->count())
                          ,
                          @endif
                            <!-- <button type="button" class="btn btn-info">{{$batch->batch_name}}</button> -->
                          @endforeach
                        @endif
                      </td>
                      <!-- <td>{{$link->subject->name??""}}</td>
                      <td>{{$link->chapter->chap_name??""}}</td>
                      <td>{{$link->type}}</td> -->
                      <td>{{$link->start_at}} </td>
                      <td>{{$link->end_at}} </td>
                      <td>{{$link->edited ? $link->edited->fullname():""}}</td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush
@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)
@section('plugins.multiselect', true)

@section('js')
<script> 
  $('#userlist').dataTable({
        "scrollX": true,
        "responsive": true,
        
        "columnDefs": [
          { "width": "70px", "targets": 1 },
          { "width": "70px", "targets": 3 },
        ]        
  });
  $('.fa.fa-copy').click(function () {
    var url = $(this).data('url');
    
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(url).select();
    document.execCommand("copy");
    $temp.remove();

    const Toast = Swal.mixin({
                  toast             : true,
                  position          : 'top-end',
                  timer             : 3000,
                  showConfirmButton : false,
                });

    Toast.fire({
        type  : 'success',
        title : 'url copy successfully'
    });
  })

  $('.select2').select2({
    theme      : 'bootstrap4',
  });

  $('.select2').each(function() {
    $( this ).parent()
            .find("input")
            .attr('placeholder',$( this ).attr('placeholder'));
  });
</script>
@stop
