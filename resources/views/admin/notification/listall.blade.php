@extends('adminlte::page')

@section('title', 'IIT-PULSE')

@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Notification</li>
        </ul>
      </div>
@stop

@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
          <a href="{{url('admin/notification/add')}}"><button type="button" class="btn btn-success">Add Message</button></a> 
                <table id="userlist" style="width: 100%" class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Message</th>
                      <th>Batch Name</th>
                      <th>Start Date</th>
                      <th>End Date</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($notifications  as $reskey => $notification)
                    <tr>
                      <td>{{$reskey+1}}</td>
                      <td>{{$notification->message}} </td>
                      <td>{{$notification->batch->batch_name}} </td>
                      <td>{{$notification->start_at}} </td>
                      <td>{{$notification->end_at}} </td>
                      <td> 
                        @if($notification->status==1) Active
                        @elseif($notification->status==0) Inactive
                        @endif
                      </td>
                      <td><a href="{{url('admin/notification/update/'.$notification->id)}}" class="edit"> 
                        <span class="oi" data-glyph="pencil"></span>
                        <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                        </i></a> &nbsp;&nbsp;
                        <a href="{{url('admin/notification/delete/'.$notification->id)}}" data-id="{{$notification->id}}" class="delete-row"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"></i></a></td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush
@section('plugins.Datatables', true)
@section('js')
<script> 
  $('#userlist').dataTable({
        "scrollX": true
  }); 
</script>
@stop
