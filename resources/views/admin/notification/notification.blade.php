@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Notification</li>
        </ul>
      </div>
@stop

@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">  
         <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Notification</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('admin/notification/save')}}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $notification->id }}">
              <!-- <input type="hidden" name="start_at" value="{{ $notification->start_at }}">
              <input type="hidden" name="end_at" value="{{ $notification->end_at }}">
               -->
                <div class="card-body">
                    <div class="form-group @if($errors->has('message')) has-error @endif">
                     @php 
                       $value = old('message',$notification->message); 
                      @endphp
                      <div class="form-group">
                        <label for="inputFname">Message</label>
                        <input type="text" class="form-control" name="message" placeholder="Enter Message" value="{{$value}}">
                      </div>
                     @if ($errors->has('message'))
                     <p class="help-block mb-0">
                        <strong>{{ $errors->first('message') }}</strong>
                     </p>
                     @endif
                    </div>

                    <div class="form-group @if($errors->has('batch_id')) has-error @endif">
                      @php 
                       $value = old('batch_id',$notification->batch_id); 
                      @endphp
                      <div class="form-group">
                        <label for="inputclass1">Batch</label>
                        <select class="custom-select" name="batch_id">
                          @foreach($batches as $batch)
                          <option @if($value == $batch->id) selected="selected" @endif value="{{$batch->id}}">
                            {{$batch->batch_name}}
                          </option>
                          @endforeach
                        </select>
                      </div>
                     @if ($errors->has('batch_id'))
                     <p class="help-block mb-0">
                        <strong>{{ $errors->first('batch_id') }}</strong>
                     </p>
                     @endif
                    </div>

                    <div class="form-group @if($errors->has('reservationtime')) has-error @endif">
                      @php 
                       $start_at = date('m/d/Y h:i A',strtotime($notification->start_at));
                       $end_at   = date('m/d/Y h:i A',strtotime($notification->end_at));
                       $reservationtime = $start_at." - ".$end_at;
                       $value = old('reservationtime',$reservationtime);
                      @endphp
                      
                      <div class="form-group">
                        <label for="inputFname">Date and Time Range:</label>

                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-clock"></i></span>
                          </div>
                          <input type="text" class="form-control float-right" id="reservationtime" name="reservationtime" required readonly value="{{$value}}">
                        </div>
                        <!-- /.input group -->
                    </div>

                     @if ($errors->has('reservationtime'))
                     <p class="help-block mb-0">
                        <strong>{{ $errors->first('reservationtime') }}</strong>
                     </p>
                     @endif
                    </div>


                    <!-- status box -->
                    <div class="form-group @if($errors->has('radioSuccess1')) has-error @endif">
                      @php 
                       $value = old('status',$notification->status); 
                      @endphp
                      
                      <div class="form-group">
                        <label for="inputFname">Status</label>

                        <div class="col-sm-7 ">
                    <!-- radio -->
                    <div class="form-group clearfix">
                      <div class="icheck-success d-inline">
                          <label for="radioSuccess1">
                            Active
                          </label>
                      </div>
                      <div class="icheck-success d-inline">
                      <input type="radio" name="radiobutton" id="radioSuccess1"  value="1" @if($notification->status == 1) checked=""  @endif>
                       
                        <label for="radioSuccess1">
                        </label>
                      </div>

                        <div class="icheck-success d-inline">
                          <label for="radioSuccess2">
                            Inactive
                          </label>
                        </div>

                      <div class="icheck-danger d-inline">
                      <input type="radio" name="radiobutton" id="radioSuccess2" value="0" @if($notification->status == 0) checked=""  @endif>
                       <label for="radioSuccess2">
                        </label>
                      </div>
                      
                    </div>
                  </div>
                        <!-- /.input group -->
                    </div>

                     @if ($errors->has('status'))
                     <p class="help-block mb-0">
                        <strong>{{ $errors->first('status') }}</strong>
                     </p>
                     @endif
                    </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
          </div>
      </div>

@stop


@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@stop



@section('plugins.dateTimePicker', true)
@section('plugins.radiobutton', true)
@section('js')
  
<script type="text/javascript">
 
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    </script>

@stop
