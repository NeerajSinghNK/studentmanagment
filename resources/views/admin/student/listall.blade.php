@extends('adminlte::page')

@section('title', 'IIT-PULSE')

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      button.batch-button{
        font-size: 13px;
        font-weight: 400;
      }
      
    </style>
@endpush
@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Student</li>
        </ul>
      </div>
@stop

@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row --> 
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
           <!-- <div id="wrapper"> -->
          <div id="wrapper">
            <div class="row">
                <div class="col-md-3 mb-4">
                    <select class="select2" id="batch">   
                      <option value=''>--Select Batch--</option>
                      <option value="{{url('admin/student/all')}}">all</option>
                      @foreach($batches as $batch)
                        <option 
                          @if(Request::fullUrl() == url('admin/student/batch/'.$batch->id)) selected="selected" @endif
                          value="{{url('admin/student/batch/'.$batch->id)}}">
                          {{$batch->batch_name}}
                        </option>
                      @endforeach
                    </select>
                </div>
                <div class="col-md-1">
                  <button id="search" type="button" class="btn btn-primary">
                    <i class="fas fa-search"></i>
                  </button>
                </div>
            </div>
          </div>
          <!-- </div> -->
           <button type="button" class="btn btn-success select-email" data-toggle="modal" data-target="#modal-default">
            Select All Email
            </button>
              <a href="{{url('admin/student/add')}}"><button type="button" class="btn btn-success">Add student</button></a>           
              <table id="userlist" style="width: 100%" class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Batch</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                @foreach($students  as $reskey => $student)
                  <tr> 
                    <td>{{$reskey+1}}</td>
                    <td>{{$student->user->first_name??""}} 
                   
                    </td>
                    <td>{{$student->user->last_name??""}} </td>
                    <td class="email">{{$student->user->email}} </td>
                    <td>
                        @foreach($student->batch as $batch)
                          <button type="button" class="btn btn-info batch-button">{{$batch->batch_name}}</button>
                        @endforeach
                      </td>
                    <td>
                    <div>
                      <input data-id="{{$student->user->id}}" class="toggle-class" type="checkbox" {{ $student->user->status ? 'checked' : '' }}>  
                    </div> 
                    </td>
                    <td><a href="{{url('admin/student/update/'.$student->id)}}" class="edit"> 
                      <span class="oi" data-glyph="pencil"></span>
                      <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                      </i></a> &nbsp;&nbsp;
                      <a href="{{url('admin/student/delete/'.$student->id)}}" data-id="{{$student->id}}" class="delete-row"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"></i></a></td>
                  </tr>
                @endforeach
                </tbody>
              </table>
        </div>
    </div>
    <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Default Modal</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">  
                  <textarea class="form-control" id='all-email'></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button id="copy-email" type="button" class="btn btn-primary">Copy</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <!-- Css Button Scroll -->
    <style>
    	#wrapper {
        margin: auto;
      }
      .bx-viewport {
        height: 40px !important;
        padding:0 10px;
        box-sizing: border-box;
        text-align: center;
      }
    </style>
<!-- End Css Button Scroll -->

@endpush
@section('plugins.Datatables', true)
@section('plugins.Sweetalert2', true)
@section('plugins.multiselect', true)
@section('plugins.toggleSwitch', true)

@section('js')
<!-- Button Scroll -->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> -->

<!-- End Button Scroll -->
<script> 
  var table = $('#userlist').dataTable({
                                      "scrollX": true,
                                      rowCallback: function (row, data) {
                                                $('.toggle-class').bootstrapToggle({
                                                    onstyle  : "success", 
                                                    offstyle : "danger", 
                                                    on       : "active", 
                                                    off      : "inactive", 
                                                    toggle   : "toggle",
                                                  });
                                            }
                                      });
  const Toast = Swal.mixin({
                  toast             : true,
                  position          : 'top-end',
                  timer             : 3000,
                  showConfirmButton : false,
                });


  $('.select-email').click(function() {
      var email = table.api().columns(3).data().eq( 0 ).join(','); // Reduce the 2D array into a 1D array of data ;
      $('#all-email').val(email);
  });

  $('#copy-email').click(function() {
      $('#all-email').select();
      document.execCommand('copy');
      Toast.fire({
        type  : 'success',
        title : 'All email copied'
      });
      $("#modal-default .close").click();
  });

  $('.toggle-class').change(function() {
      var status  = $(this).prop('checked') == true ? 1 : 0; 
      var user_id = $(this).data('id'); 
      var baseUrl = "{{url('')}}";
      
      $.ajax({
          url     : baseUrl+ '/admin/user/changestatus/' +user_id,
          type    : "GET",
          dataType: "json",
          data    : {'status': status, 'user_id': user_id},
          success : function(result){
            Toast.fire({
              type  : result.type,
              title : result.message
            });
         }
      })
  });
  
  $('.select2').select2({
      theme: 'bootstrap4'
  });
  
  $("#search").click(function () {
    var batch = $('#batch').val();
    if(batch != null && batch != ""){
      window.location.href = batch; 
    }
  });

</script>
@stop
