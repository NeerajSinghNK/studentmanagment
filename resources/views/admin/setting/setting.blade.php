@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<!-- <h1>Dashboard</h1> -->
<div class="app-title">
  <div>
    <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
  </div>
  <ul class="app-breadcrumb breadcrumb">
  <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
        <li class="breadcrumb-item active">Setting</li>
  </ul>
</div>
@stop

@section('content')
<!-- <p>Welcome to this beautiful admin panel.</p> -->
 
<div class="container-fluid">
  <div class="row">
    <div class="col-md-3">
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Setting</h3>
              </div>

            <div class="tab-pane" id="settings">
              <form role="form" action="{{url('admin/setting/save')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}                  
                <div class="card-body">
                  <div class="form-group @if($errors->has('value')) has-error @endif">
                    
                    <div class="form-group">
                      <label for="inputFname">Value</label>
                      <input type="text" class="form-control" name="title" value="{{old('title',$settings['title'])}}"placeholder="Enter Title">
                    </div>
                    @if ($errors->has('title'))
                    <p class="help-block mb-0">
                      <strong>{{ $errors->first('title') }}</strong>
                    </p>
                    @endif
                 
                  <div class="input-group">
                    <!-- <div class='custom-file'> -->
                        <!-- <label  for="inputFname"> Choose Image</label> -->
                        <input type="hidden" name="logo" value="{{old('title',$settings['logo'])}}">
                        <input type="file"  name="logofile" >

                      </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
              <!-- <form class="form-horizontal">
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputName" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputName2" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputName2" placeholder="Name">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputExperience" class="col-sm-2 col-form-label">Experience</label>
                        <div class="col-sm-10">
                          <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Skills</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <div class="checkbox">
                            <label>
                              <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form> -->
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->
  </div>
</div>
@stop

@push('css')
<link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush

@section('js')
<script> console.log('Hi!'); </script>
@stop 