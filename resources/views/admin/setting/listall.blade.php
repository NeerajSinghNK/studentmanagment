@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <!-- <h1>Dashboard</h1> -->
    <div class="app-title">
        <div>
          <h1><i class="fa fa-dashboard"></i>Admin Settings</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ul>
      </div>
@stop

@section('content')
  <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
          <div class="col-md-12">
          </div>
              <table id="userlist" style="width: 100%" class="table table-bordered">
                <thead>                  
                  <tr>
                    <th style="width: 10px">#</th>
                    <th> Name</th>
                    <th>Value</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                 @foreach($website_setting  as $reskey => $website_settin)
                  <tr>
                    <td>{{$reskey+1}}</td>
                    <td>{{$website_settin->name}} </td>
                    <td>
                      @if($website_settin->name=='logo')
                       <img src="{{asset('storage/'.$website_settin->value)}}" width="150px" height="100px"> 
                       @else
                       {{$website_settin->value}} 
                      @endif
                    </td>
                    <td>
                      <a href="{{url('admin/setting/upload/'.$website_settin->id)}}" class="edit"> 
                        <span class="oi" data-glyph="pencil"></span>
                        <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                        </i>
                      </a>
                    </td>
                  </tr>
                @endforeach 
                </tbody>
              </table>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush

@section('js')
    <script> console.log('Hi!'); </script>
@stop
