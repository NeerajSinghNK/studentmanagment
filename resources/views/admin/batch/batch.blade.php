@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<!-- <h1>Dashboard</h1> -->
<div class="app-title">
  <div>
    <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
  </div>
  <ul class="app-breadcrumb breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
    <li class="breadcrumb-item active">Batch</li>
  </ul>
</div>
@stop

@section('content')
<!-- <p>Welcome to this beautiful admin panel.</p>  -->

<div class="container-fluid">
  <!-- Small boxes (Stat box) -->
  <div class="row">
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Add Batch</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="{{url('admin/batch/save')}}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="{{ $batch->id }}">
          <div class="card-body">
            <div class="form-group @if($errors->has('batch_name')) has-error @endif">
              @php
              $value = old('batch_name',$batch->batch_name);
              @endphp
              <div class="form-group">
                <label for="inputFname">Batch Name</label>
                <input type="text" class="form-control" name="batch_name" placeholder="Enter Batch Name"
                  value="{{$value}}">
              </div>
              @if ($errors->has('batch_name'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('batch_name') }}</strong>
              </p>
              @endif
            </div>
            <div class="form-group @if($errors->has('class')) has-error @endif">
              @php
              $value = old('class',$batch->class);
              @endphp
              <div class="form-group">
                <label for="inputclass1">Class</label>
                <select class="custom-select" name="class">
                  <option @if($value=="class 9th" ) selected="selected" @endif value="class 9th">class 9th</option>
                  <option @if($value=="class 10th" ) selected="selected" @endif value="class 10th">class 10th</option>
                  <option @if($value=="class 11th" ) selected="selected" @endif value="class 11th">class 11th</option>
                  <option @if($value=="class 12th" ) selected="selected" @endif value="class 12th">class 12th</option>
                  <option @if($value=="Dropper" ) selected="selected" @endif value="Dropper">Dropper</option>
                </select>
              </div>
              @if ($errors->has('class'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('class') }}</strong>
              </p>
              @endif
            </div>
            <div class="form-group @if($errors->has('subject_ids')) has-error @endif">
              @php $value = old('subject_ids',$batch->subjects->pluck('id')->toArray()); @endphp
              <div class="form-group">
                <label for="inputFname">subjects</label>
                <select class="select2" multiple data-placeholder="Select a Subject" name="subject_ids[]" style="width: 100%;">
                  @foreach($subjects as $subject)
                  <option @if(in_array($subject->id,$value)) selected="selected" @endif value="{{$subject->id}}">
                    {{$subject->name}}</option>
                  @endforeach
                </select>
              </div>
              @if ($errors->has('subject_id'))
              <p class="help-block mb-0">
                <strong>{{ $errors->first('subject_id') }}</strong>
              </p>
              @endif
            </div>
            <div class="form-group @if($errors->has('subject_id')) has-error @endif">
              @php
              $value = old('subject_id',$batch->subject_id);
              @endphp
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
        </form>
      </div>
    </div>
  </div>

  @stop

  @push('css')
  <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
  @endpush
  @section('plugins.multiselect', true)
  @section('js')

  <script>
    console.log("hello");
    //Initialize Select2 Elements
    $('.select2').select2({
      theme: 'bootstrap4'
    }) 
  </script>
  @stop