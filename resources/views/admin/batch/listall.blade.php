@extends('adminlte::page')

@section('title', 'IIT-PULSE')

@section('content_header')
   
    <div class="app-title">
        <div>
          <!-- <h1><i class="fa fa-dashboard"></i> Dashboard</h1> -->
        </div>
        <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><a href="{{url('admin/dashboard')}}">Dashboard</a></li>
          <li class="breadcrumb-item active">Batch</li>
        </ul>
      </div>
@stop

@section('content')
    <!-- <p>Welcome to this beautiful admin panel.</p>  -->

    <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
        <div class="card-body">
          <a href="{{url('admin/batch/add')}}"><button type="button" class="btn btn-success">Add batch</button></a> 
                <table id="userlist" style="width: 100%" class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Batch Name</th>
                      <th>Subject Name</th>
                      <th>Class</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @foreach($batchs  as $reskey => $batch)
                    <tr>
                      <td>{{$reskey+1}}</td>
                      <td>{{$batch->batch_name}} </td>
                      <td>
                        @foreach($batch->subjects as $subject)
                          <button type="button" class="btn btn-info">{{$subject->name}}</button>
                        @endforeach
                      </td>
                      <td>{{$batch->class}} </td>
                      <!--<td>{{$batch->subject_id}} </td>-->
                      <!--
                        @foreach($batch->subjects as $subject)
                        {{$subject->name}}
                        @endforeach
                      -->
                      <td><a href="{{url('/admin/batch/update/'.$batch->id)}}" class="edit"> 
                        <span class="oi" data-glyph="pencil"></span>
                        <i class="fas fa-pencil-alt" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Edit">
                        </i></a> &nbsp;&nbsp;
                        <a onclick="return confirm('Are you sure,you want to delete? ')" href="{{url('admin/batch/delete/'.$batch->id)}}"   data-id="{{$batch->id}}" class="delete-row"><i class="fa fa-trash" aria-hidden="true" data-toggle="tooltip" title="" data-original-title="Delete"></i></a></td>
                    </tr>
                  @endforeach
                  </tbody>
                </table>
        </div>
    </div>
@stop

@push('css')
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@endpush
@section('plugins.Datatables', true)
@section('js')
<script> 
  $('#userlist').dataTable({
        "scrollX": true
  }); 
</script>
@stop
