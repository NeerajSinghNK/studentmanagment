@php
    $user = auth()->user();
@endphp
<form role="form" action="{{url('/admin/saveAdminProfile')}}" method="post" enctype="multipart/form-data">
  {{ csrf_field() }}
  
  <div class="card-body">
    <div class="form-group @if($errors->has('first_name')) has-error @endif">
      @php
      $value = old('first_name',$user->first_name);
      @endphp
      <div class="form-group">
        <label for="inputFname">Firstname</label>
        <input type="text" class="form-control" name="first_name" id="inputFname" value='{{$value}}'
          placeholder="Enter Firstname">
      </div>
      @if ($errors->has('first_name'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('first_name') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('last_name')) has-error @endif">
      @php
      $value = old('last_name',$user->last_name);
      @endphp
      <div class="form-group">
        <label for="inputLname">Lastname</label>
        <input type="text" class="form-control" name="last_name" id="inputLname" value='{{$value}}'
          placeholder="Enter Lastname">
      </div>
      @if ($errors->has('last_name'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('last_name') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('email')) has-error @endif">
      @php
      $value = old('email',$user->email);
      @endphp
      <div class="form-group">
        <label for="inputEmail1">Email</label>
        <input type="email" class="form-control" name="email" id="inputEmail1" value='{{$value}}'
          placeholder="Enter email">
      </div>
      @if ($errors->has('email'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('email') }}</strong>
      </p>
      @endif
    </div>
   
   

    <div class="form-group @if($errors->has('country')) has-error @endif">
      @php
      $value = old('country',$user->country);
      @endphp
      <div class="form-group">
        <label for="inputCountry">Country</label>
        <select class="custom-select" name="country">
         
          <option selected="selected" value="India">
           India
          </option>
         
        </select>
      </div>
      @if ($errors->has('country'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('country') }}</strong>
      </p>
      @endif
    </div>
  

    <div class="form-group @if($errors->has('state')) has-error @endif">
      @php
      $value = old('state',$user->state);
      @endphp
      <div class="form-group">
        <label for="inputState">State</label>
        <input type="text" class="form-control" name="state" id="state" value='{{$value}}'
          placeholder="State">
         
      </div>
      @if ($errors->has('state'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('state') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('city')) has-error @endif">
      @php
      $value = old('city',$user->city);
      @endphp
      <div class="form-group">
        <label for="inputCity">City</label>
        <input type="text" class="form-control" name="city" id="city" value='{{$value}}'
          placeholder="City">
         
      </div>
      @if ($errors->has('city'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('city') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('pincode')) has-error @endif">
      @php
      $value = old('pincode',$user->pincode);
      @endphp
      <div class="form-group">
        <label for="inputPincode">Pincode</label>
        <input type="text" class="form-control" name="pincode" id="pincode" value='{{$value}}'
          placeholder="Pincode">
         
      </div>
      @if ($errors->has('pincode'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('pincode') }}</strong>
      </p>
      @endif
    </div>

    <div class="form-group @if($errors->has('village')) has-error @endif">
      @php
      $value = old('village',$user->village);
      @endphp
      <div class="form-group">
        <label for="inputVillage">Village</label>
        <input type="text" class="form-control" name="village" id="village" value='{{$value}}'
          placeholder="village">
         
      </div>
      @if ($errors->has('village'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('village') }}</strong>
      </p>
      @endif
    </div>



    <div class="form-group @if($errors->has('road')) has-error @endif">
      @php
      $value = old('road',$user->road);
      @endphp
      <div class="form-group">
        <label for="inputRoad">Road</label>
        <input type="text" class="form-control" name="road" id="road" value='{{$value}}'
          placeholder="Road">
         
      </div>
      @if ($errors->has('road'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('road') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('colony')) has-error @endif">
      @php
      $value = old('colony',$user->colony);
      @endphp
      <div class="form-group">
        <label for="inputColony">Colony</label>
        <input type="text" class="form-control" name="colony" id="colony" value='{{$value}}'
          placeholder="Colony">
         
      </div>
      @if ($errors->has('colony'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('colony') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('house_no')) has-error @endif">
      @php
      $value = old('house_no',$user->house_no);
      @endphp
      <div class="form-group">
        <label for="inputHouseNo">House No</label>
        <input type="text" class="form-control" name="house_no" id="house_no" value='{{$value}}'
          placeholder="House No.">
         
      </div>
      @if ($errors->has('house_no'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('house_no') }}</strong>
      </p>
      @endif
    </div>


    <div class="form-group @if($errors->has('landmark')) has-error @endif">
      @php
      $value = old('landmark',$user->landmark);
      @endphp
      <div class="form-group">
        <label for="inputLandmark">Landmark</label>
        <input type="text" class="form-control" name="landmark" id="landmark" value='{{$value}}'
          placeholder="Landmark">
         
      </div>
      @if ($errors->has('landmark'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('landmark') }}</strong>
      </p>
      @endif
    </div>

      <div class="form-group @if($errors->has('contact')) has-error @endif">
        @php $value = old('contact',$user->contact); @endphp
        <div class="form-group">
          <label for="contact">Contact</label>
          <input type="text" class="form-control" name="contact" id="contact" value='{{$value}}'
            placeholder="contact">
           
        </div>
        @if ($errors->has('contact'))
        <p class="help-block mb-0">
          <strong>{{ $errors->first('contact') }}</strong>
        </p>
        @endif
      </div>


    <div class="form-group  @if($errors->has('password')) has-error @endif">
      <div class="form-group">
        <label for="inputPassword">Password</label>
        <input type="password" class="form-control" name="password" id="inputPassword" placeholder="Enter Password" value="">
      </div>
      @if ($errors->has('password'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('password') }}</strong>
      </p>
      @endif
    </div> 

    <div class="form-group  @if($errors->has('password_confirmation')) has-error @endif">
      <div class="form-group">
        <label for="inputconpassword">Confirm Password</label>
        <input type="password" class="form-control" name="password_confirmation" id="inputconpassword"
          placeholder="Confirm Password">
      </div>
      @if ($errors->has('password_confirmation'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('password_confirmation') }}</strong>
      </p>
      @endif
    </div>
  </div>
               
  <div class="form-group  @if($errors->has('image')) has-error @endif">
      <div class="input-group">
        <div class="custom-file col-md-7">
          <input type="hidden" name="profile_pic" value="{{$user->image}}">
          <input type="file" name="image" class="custom-file-input ">
          <lable id="pic" class="custom-file-label">Choose Profile Picture</lable>
        </div>
      </div>
      @if ($errors->has('image'))
      <p class="help-block mb-0">
        <strong>{{ $errors->first('image') }}</strong>
      </p>
      @endif
    </div>

  <!-- /.card-body -->
  <div class="card-footer">
    <button type="submit" class="btn btn-primary">Submit</button>
  </div>
</form>


