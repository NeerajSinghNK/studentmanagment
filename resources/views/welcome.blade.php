<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                      @if(Auth::user()->role_id==1 || Auth::user()->role_id==2 )
                        <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
                       @endif
                        @if(Auth::user()->role_id==3)
                        <a href="{{ url('/student/dashboard') }}">Dashboard</a>
                       @endif
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        <!-- @if (Route::has('register')) -->
                            <!-- <a href="{{ route('register') }}">Register</a> -->
                        <!-- @endif -->
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    IIT-Pulse
                </div>

              <!--   <div class="links">
                    <a href="https://laravel.com/docs">Docs</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://blog.laravel.com">Blog</a>
                    <a href="https://nova.laravel.com">Nova</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://vapor.laravel.com">Vapor</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
    </body> -->



<head>
    <!-- <title>Student Login</title> -->
    <meta charset="utf-8">
    <link rel="icon" type="image/jpg" href="{{url('dist/img/logo.png')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    
    <style type="text/css">


        .logoTitle{
            width: 100%;
            height: 100%;
        }

        @media screen and (max-width: 431px) {
            .pageTitle{
                font-size: 23px;
            }
        }

        /*@media screen and (min-width: 1020px) {
            .logoTitle{
                margin-left: -10000px;
                background-color: red;

            }
        }
*/
    </style>
</head>
<body background="https://www.iitpulse.com/test/assets/img/iitp.png">
    <div class="container">
        <div class="row ">
            <div style="display:table-cell; vertical-align:middle; text-align:center">
            <img src="https://www.iitpulse.com/test/assets/img/iitpulse_logo.png" class="m-1 col-lg-3  rounded mx-auto " id='logo1' >
             </div>
        </div>  
        <div class="row ">

            <div class="h2 col-lg-12 pageTitle">Online Portal | <b style="color:#858080;">Student</b></div>
        </div>
        <!-- <div class="row"> -->

            <!-- <div class="col-sm-6" >
                <img class="d-block w-100" src="https://www.iitpulse.com/test/poster_img/img1558942442.jpg" alt="First slide"> -->
                <!-- Carausel-->
                <!-- <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://www.iitpulse.com/test/poster_img/img1558942442.jpg" alt="First slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.iitpulse.com/test/poster_img/img1558942442.jpg" alt="Second slide">
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://www.iitpulse.com/test/poster_img/img1558942442.jpg" alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div> -->
                <!-- Carausel-->
                <!-- </div> -->
            </div>
        </div>
        <script type="text/javascript">

            if ($(window).width() > 1000) {
                $('#logo1').removeClass('mx-auto');
            }



        </script><script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://use.fontawesome.com/a0aac8df13.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap.min.css" rel="stylesheet"/>


<style type="text/css">
    .carousel {
        margin: 1.5rem;

    }
    .carousel-inner {
        height: auto;

        /*background-color: red;*/
    }

    .carousel-control-prev {
        margin-left: -50px;
    }

    .carousel-control-next {
        margin-right: -50px;
    }
</style>


                <!-- login window end -->

                <div class="col-sm-12 m-1 sign-up shadow-lg" style="display: none;">
                    <form method="POST"  class="sign-up" action="https://www.iitpulse.com/test/student/Student/studentSignUp">
                        <div class="row">
                            <div class="col-sm-12 p-3 h3 rounded-top font-weight-light  text-light text-center" style="background-color: #058728">
                                Student Sign Up
                                <button type="button" class="close text-white" onclick="$('div.sign-up').hide();$('#hidelogin').show();">&times;</button>
                            </div>
                            <div class="ml-2 mr-2 row">
                                <div class="col-sm-6 p-1">
                                    <label for="student-name"><small>Full Name<sup class="text-danger">*</sup></small></label>
                                    <input type="text" id="student-name" class="form-control" placeholder="Enter full name" required autofocus name="student-name"  autocomplete="off">
                                </div>
                                <div class="col-sm-6 p-1">
                                    <label for="student-mobile"><small>Mobile Number<sup class="text-danger">*</sup></small></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">+91</div>
                                        </div>
                                        <input type="number" id="student-mobile" class="form-control small" placeholder="Mobile number" required name="student-mobile"  autocomplete="off">
                                    </div>
                                    <small class="text-danger font-italic mobile-warning" style="display: none;">Incorrect Mobile Number</small>
                                </div>
                                <div class="col-sm-6 p-1">
                                    <label for="password"><small>Password<sup class="text-danger">*</sup></small></label>
                                    <input type="password" id="password" class="form-control p-2" placeholder="Enter password" required name="password">
                                    <small class="text-danger font-italic password-length-warning" style="display: none;">Password must be alteast 6 characters long!</small>
                                </div>
                                <div class="col-sm-6 p-1">
                                    <label for="re-password"><small>Confirm Password<sup class="text-danger">*</sup></small></label>
                                    <input type="password" id="re-password" class="form-control" placeholder="Re-enter password" required name="re-password">
                                    <small class="text-danger font-italic password-warning" style="display: none;">Passwords DO NOT match!</small>
                                </div>
                                <div class="col-sm-6 p-1">
                                    <label for="student-state"><small>Select State<sup class="text-danger">*</sup></small></label>
                                    <select id="student-state" name="student-state" class="form-control">
                                    </select>
                                </div>
                                <div class="col-sm-6 p-1">
                                    <label for="student-city"><small>Select City<sup class="text-danger">*</sup></small></label>
                                    <select id="student-city" name="student-city" class="form-control">
                                    </select>
                                </div>

                                <div class="col-sm-4 p-1">
                                    <label for="student-class"><small>Select Class<sup class="text-danger">*</sup></small></label>
                                    <select id="student-class" name="student-class" class="form-control">
                                        <option value="09">9&#x1D57;&#x02B0; (IX)</option>
                                        <option value="10">10&#x1D57;&#x02B0; (X)</option>
                                        <option value="11">11&#x1D57;&#x02B0; (XI)</option>
                                        <option value="12">12&#x1D57;&#x02B0; (XII)</option>
                                        <option value="13">Dropper</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 p-1">
                                    <label for="student-medium"><small>Select Medium<sup class="text-danger">*</sup></small></label>
                                    <select id="student-medium" name="student-medium" class="form-control">
                                        <option value="1">English</option>
                                        <option value="0">Hindi</option>
                                    </select>
                                </div>
                                <div class="col-sm-4 p-1">
                                    <label for="student-stream"><small>Select Stream<sup class="text-danger">*</sup></small></label>
                                    <div class="small text-danger font-italic" id="no-stream-for-9-10" style="display: none;">Stream does not apply for <b>9&#x1D57;&#x02B0;</b> &  <b>10&#x1D57;&#x02B0;</b> class students.</div>
                                    <select id="student-stream" name="student-stream" class="form-control">
                                        <option value="1">Mathematics</option>
                                        <option value="0">Biology</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label class="checkbox-inline small">
                                    <input class="test-result-date" type="checkbox" required>
                                    I accept the <a href="https://www.iitpulse.com/test/student/Student/termsAndConditions" target="_blank">Terms and Conditions</a> of IIT-PULSE.
                                </label>
                            </div>
                            <div class="col-sm-12 pl-4 pr-4 mt-2 mb-2">
                                <button type="submit" class="btn  btn-block sign-up font-weight-light  text-light" type="submit" style="background-color: #058728">Sign Up/Register <i class='fas fa-user-plus'></i></button>

                            </div>
                        </div>
                    </form>
                </div> 
            </div>
        </div>

        <!-- sign up window end -->
        <!-- <div class="col-sm-20"> -->
            <!-- Carausel start-->
            <div class="container">

                <div id="carouselExample" class="carousel slide" data-ride="carousel" data-interval="2000">
                    <div class="carousel-inner row w-100 mx-100" role="listbox">
                         <div style="display:table-cell; vertical-align:middle; text-align:center">
                                                            <div class="carousel-item active">
                                    <img src="https://www.iitpulse.com/test/poster_img/img1558942442.jpg" alt="IIT-PULSE" class="img-fluid" style="height:780px;"  >
                                </div>
                                                                <div class="carousel-item ">
                                    <img src="https://www.iitpulse.com/test/poster_img/img1589197806.jpeg" alt="IIT-PULSE" class="img-fluid" >
                                </div>
                                                                <div class="carousel-item ">
                                    <img src="https://www.iitpulse.com/test/poster_img/img1589197814.jpeg" alt="IIT-PULSE" class="img-fluid" >
                                </div>
                               </div>                     </div>
                    <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                        <i style="color:#030970; text-decoration: none;" class="fa fa-chevron-circle-left fa-2x"></i>

                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
                        <i style="color:#030970; text-decoration: none;" class="fa fa-chevron-circle-right fa-2x"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- <div class="clearfix"></div> -->

        <!-- </div> -->
    </div>
</div>
<!-- Carausel End-->
<script type="text/javascript">
    $(document).ready(function () {

        $("input#student-mobile").focusout(function() {
            if ($(this).val().length != 10) {
                $("small.mobile-warning").show();
                $("button.sign-up").attr('disabled', "");
            }
            else {
                $("small.mobile-warning").hide();
                $("button.sign-up").removeAttr('disabled');
            }
        });

        $("select#student-class").change(function () {
            if ($(this).find("option:selected").val() <= 10) {
                $("select#student-stream").hide();
                $("div#no-stream-for-9-10").show();
            }
            else {
                $("select#student-stream").show();
                $("div#no-stream-for-9-10").hide();
            }
        });

        $("input#password").focusout(function() {
            if ($(this).val().length < 6) {
                $("small.password-length-warning").show();
                $("button.sign-up").attr('disabled', "");
            }
            else {
                $("small.password-length-warning").hide();
                $("button.sign-up").removeAttr('disabled');
            }
        });

        $("input#password").keyup(function() {
            if (($("input#re-password").val().length > 0) && ($(this).val() != $("input#re-password").val())) {
                $("small.password-warning").show();
                $("button.sign-up").attr('disabled', "");
            }
            else {
                $("small.password-warning").hide();
                $("button.sign-up").removeAttr('disabled');
            }

        });

        $("input#re-password").keyup(function() {
            if ($(this).val() != $("input#password").val()) {
                $("small.password-warning").show();
                $("button.sign-up").attr('disabled', "");
            }
            else {
                $("small.password-warning").hide();
                if (($("input#re-password").val().length < 6)) {
                    $("small.password-length-warning").show();
                }
                else {
                    $("small.password-length-warning").hide();
                    $("button.sign-up").removeAttr('disabled');
                }
            }
        });

        var cities = [];
        function loadStatesCities (s_id, c_id) {
            $.get('https://www.iitpulse.com/test/StateCityList/getStateCityList?type=state', function(data) {
                for (e of data) {
                    $("select#student-state").append(`<option value=${e.id}>${e.state_name}</option>`);
                }
                $("select#student-state").val(s_id);
                cities = [];
                $.get(`https://www.iitpulse.com/test/StateCityList/getStateCityList?type=city`, function(data) {
                    for (state_id in data) {
                        var city = ``;
                        for (city_data of data[state_id]) {
                            city += `<option value="${city_data.id}">${city_data.city_name}</option>`;
                        }
                        cities[state_id] = city;
                    }
                    $('select#student-city').html(cities[s_id]).val(c_id);
                }, 'json');
            }, 'json');
        }

        $("select#student-state").on("change", function () {
            var id = $("select#student-state").children('option:checked').val();
            $("select#student-city").html(cities[id]);
        });

        loadStatesCities('21', '2229');$('select#student-class').val(12);   });
</script>






<div class="container style="height: 100%;">
    <hr>
    <div class="row ">
        <div class="col-lg-4 offset-lg-1">
            <h4 class="mb-1 ">About</h4><hr>
            <div class="text-justify">IIT-PULSE is a coaching institute in Indore preparing students for JEE MAIN AND ADVANCED, NEET AND AIIMS, Olympiads and respective Boards (Schools).</div>
        </div>
        <div class="col-lg-5 offset-lg-2">
            <h4 class="mb-1 ">Contact</h4><hr>
            <h6><a class="text-dark" style="cursor: pointer; text-decoration: none !important" href="https://www.google.co.in/maps/place/IIT+PULSE+-+Best+IIT-JEE+Coaching+in+Indore%2F+Best+NEET+Coaching+In+indore+,+IIT+JEE+main+advanced+%2F+AIIMS+PMT+%2F+NEET+%2F+AIPMT+coaching+classes%2Fbest+coaching+in+Indore%2Fcoaching+in+indore/@22.7194302,75.8854916,19z/data=!4m5!3m4!1s0x3962fd3bdec94d2f:0xb73c95c458bf71d0!8m2!3d22.719763!4d75.884231"><i class="fas fa-map-marker-alt"></i>&nbsp;&nbsp;&nbsp;&nbsp;106, TBC Tower, Geeta Bhavan Square, Indore</a></h6>
            <h6><a class="text-dark" style="cursor: pointer; text-decoration: none !important" href="mailto:iitpulseindore@gmail.com"><i class="fas fa-envelope"></i>&nbsp;&nbsp;&nbsp;&nbsp;iitpulseindore@gmail.com</a></h6>
            <h6><div><i class="fas fa-phone"></i>&nbsp;+91-98937 04288, +91-78698 02888</div></h6>
            <h3><a href="https://www.youtube.com/channel/UC5eEP41hNOSEDnlDlhwARdg?&ab_channel=IITPULSE"><i class="fab fa-youtube text-danger"></i></a>&nbsp;&nbsp;<a href="https://www.facebook.com/IITPULSE.INDORE"><i class="fab fa-facebook-square text-primary"></i></a>&nbsp;&nbsp;<a href=""><i class="fab fa-google" style="color: #dd4b39" ></i></a></h3>
        </div>
    </div>
</div>
</body>
</html>