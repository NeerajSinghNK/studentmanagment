<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            
            $table->string('first_name')->nullable()->change();
            $table->string('last_name')->nullable()->change();
            $table->string('country')->nullable()->change();
            $table->string('state')->nullable()->change();
            $table->string('city')->nullable()->change();
            $table->string('pincode')->nullable()->change();
            $table->string('village')->nullable()->change();
            $table->string('road')->nullable()->change();
            $table->string('colony')->nullable()->change();
            $table->string('house_no')->nullable()->change();
            $table->string('landmark')->nullable()->change();
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
        
            $table->string('first_name')->change();
            $table->string('last_name')->change();
            $table->string('country')->change();
            $table->string('state')->change();
            $table->string('city')->change();
            $table->string('pincode')->change();
            $table->string('village')->change();
            $table->string('road')->change();
            $table->string('colony')->change();
            $table->string('house_no')->change();
            $table->string('landmark')->change();
        
        });
    }
}
