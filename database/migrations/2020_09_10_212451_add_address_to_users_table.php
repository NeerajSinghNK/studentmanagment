<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAddressToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('house_no');
            $table->string('colony');
            $table->string('landmark');
            $table->string('road');
            $table->string('village');
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('pincode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('house_no');
            $table->dropColumn('colony');
            $table->dropColumn('landmark');
            $table->dropColumn('road');
            $table->dropColumn('village');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('country');
            $table->dropColumn('pincode');
        });
    }
}
