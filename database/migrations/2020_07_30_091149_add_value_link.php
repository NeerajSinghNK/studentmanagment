<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddValueLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('links', function (Blueprint $table) {
            $table->bigInteger('batch_id')->unsigned();
            $table->foreign('batch_id')
                ->references('id')
                ->on('batch')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('links', function (Blueprint $table) {
            // 1. Drop foreign key constraints
            $table->dropForeign(['batch_id']);
            // 2. Drop the column
            $table->dropColumn('batch_id');
        });
    }
}
