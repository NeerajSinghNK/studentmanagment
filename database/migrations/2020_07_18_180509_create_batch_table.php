<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batch', function (Blueprint $table) {
            $table->id(); 
            $table->char('batch_name', 100);
            $table->char('class', 100);   
            $table->char('language', 100);
            $table->char('stream', 100);
            $table->char('remarks', 100);
            $table->date('batch_end_date'); 
            $table->date('batch_start_date'); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch');
    }
}
