<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('links', function (Blueprint $table) {
            $table->dateTime('start_at');
            $table->dateTime('end_at');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('links', function (Blueprint $table) {
            // 1. Drop foreign key constraints
           // $table->dropForeign(['batch_id']);
            // 2. Drop the column
            $table->dropColumn('start_at');
            $table->dropColumn('end_at');
        });
    }
}
