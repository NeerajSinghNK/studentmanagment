<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameBatchIdInLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('links', function (Blueprint $table) {
            $table->dropForeign('links_batch_id_foreign');
            $table->renameColumn('batch_id', 'subject_id');
            $table->foreign('subject_id')
                ->references('id')
                ->on('subjects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('links', function (Blueprint $table) {
            $table->dropForeign('links_subject_id_foreign');
            $table->renameColumn('subject_id', 'batch_id');
            $table->foreign('batch_id')
                ->references('id')
                ->on('batch ');
        });
    }
}
