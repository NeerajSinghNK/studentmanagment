<?php

use Illuminate\Database\Seeder;

class BatchSubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$BatchSubjects = array (
		  0 => 
		  array (
		    'id' => '1',
		    'subject_id' => '1',
		    'batch_id' => '1',
		    'created_at' => '2020-08-25 11:46:03',
		    'updated_at' => '2020-08-25 11:46:03',
		  ),
		  1 => 
		  array (
		    'id' => '2',
		    'subject_id' => '2',
		    'batch_id' => '1',
		    'created_at' => '2020-08-25 11:46:03',
		    'updated_at' => '2020-08-25 11:46:03',
		  ),
		  2 => 
		  array (
		    'id' => '3',
		    'subject_id' => '3',
		    'batch_id' => '1',
		    'created_at' => '2020-08-25 11:46:03',
		    'updated_at' => '2020-08-25 11:46:03',
		  ),
		  3 => 
		  array (
		    'id' => '4',
		    'subject_id' => '1',
		    'batch_id' => '2',
		    'created_at' => '2020-08-25 11:46:53',
		    'updated_at' => '2020-08-25 11:46:53',
		  ),
		  4 => 
		  array (
		    'id' => '5',
		    'subject_id' => '2',
		    'batch_id' => '2',
		    'created_at' => '2020-08-25 11:46:53',
		    'updated_at' => '2020-08-25 11:46:53',
		  ),
		  5 => 
		  array (
		    'id' => '6',
		    'subject_id' => '3',
		    'batch_id' => '2',
		    'created_at' => '2020-08-25 11:46:53',
		    'updated_at' => '2020-08-25 11:46:53',
		  ),
		  6 => 
		  array (
		    'id' => '7',
		    'subject_id' => '2',
		    'batch_id' => '3',
		    'created_at' => '2020-08-25 14:34:43',
		    'updated_at' => '2020-08-25 14:34:43',
		  ),
		  7 => 
		  array (
		    'id' => '8',
		    'subject_id' => '1',
		    'batch_id' => '4',
		    'created_at' => '2020-08-28 16:02:16',
		    'updated_at' => '2020-08-28 16:02:16',
		  ),
		  8 => 
		  array (
		    'id' => '9',
		    'subject_id' => '2',
		    'batch_id' => '4',
		    'created_at' => '2020-08-28 16:02:16',
		    'updated_at' => '2020-08-28 16:02:16',
		  ),
		  9 => 
		  array (
		    'id' => '10',
		    'subject_id' => '4',
		    'batch_id' => '4',
		    'created_at' => '2020-08-28 16:02:31',
		    'updated_at' => '2020-08-28 16:02:31',
		  ),
		  10 => 
		  array (
		    'id' => '11',
		    'subject_id' => '1',
		    'batch_id' => '5',
		    'created_at' => '2020-08-28 16:03:03',
		    'updated_at' => '2020-08-28 16:03:03',
		  ),
		  11 => 
		  array (
		    'id' => '12',
		    'subject_id' => '2',
		    'batch_id' => '5',
		    'created_at' => '2020-08-28 16:03:03',
		    'updated_at' => '2020-08-28 16:03:03',
		  ),
		  12 => 
		  array (
		    'id' => '13',
		    'subject_id' => '3',
		    'batch_id' => '5',
		    'created_at' => '2020-08-28 16:03:03',
		    'updated_at' => '2020-08-28 16:03:03',
		  ),
		  13 => 
		  array (
		    'id' => '14',
		    'subject_id' => '1',
		    'batch_id' => '6',
		    'created_at' => '2020-08-28 16:03:59',
		    'updated_at' => '2020-08-28 16:03:59',
		  ),
		  14 => 
		  array (
		    'id' => '15',
		    'subject_id' => '2',
		    'batch_id' => '6',
		    'created_at' => '2020-08-28 16:03:59',
		    'updated_at' => '2020-08-28 16:03:59',
		  ),
		  15 => 
		  array (
		    'id' => '16',
		    'subject_id' => '4',
		    'batch_id' => '6',
		    'created_at' => '2020-08-28 16:03:59',
		    'updated_at' => '2020-08-28 16:03:59',
		  ),
		  16 => 
		  array (
		    'id' => '17',
		    'subject_id' => '1',
		    'batch_id' => '7',
		    'created_at' => '2020-08-28 16:05:05',
		    'updated_at' => '2020-08-28 16:05:05',
		  ),
		  17 => 
		  array (
		    'id' => '18',
		    'subject_id' => '2',
		    'batch_id' => '7',
		    'created_at' => '2020-08-28 16:05:05',
		    'updated_at' => '2020-08-28 16:05:05',
		  ),
		  18 => 
		  array (
		    'id' => '19',
		    'subject_id' => '4',
		    'batch_id' => '7',
		    'created_at' => '2020-08-28 16:05:05',
		    'updated_at' => '2020-08-28 16:05:05',
		  ),
		  19 => 
		  array (
		    'id' => '20',
		    'subject_id' => '1',
		    'batch_id' => '8',
		    'created_at' => '2020-09-10 18:43:25',
		    'updated_at' => '2020-09-10 18:43:25',
		  ),
		  20 => 
		  array (
		    'id' => '21',
		    'subject_id' => '2',
		    'batch_id' => '8',
		    'created_at' => '2020-09-10 18:43:25',
		    'updated_at' => '2020-09-10 18:43:25',
		  ),
		  21 => 
		  array (
		    'id' => '23',
		    'subject_id' => '4',
		    'batch_id' => '8',
		    'created_at' => '2020-09-10 18:43:48',
		    'updated_at' => '2020-09-10 18:43:48',
		  ),
		);

		foreach ($BatchSubjects as $key => $BatchSubject) {
			DB::table('batch_subject')->updateOrInsert(['id'=>$BatchSubject['id']],$BatchSubject);
		}
    }
}