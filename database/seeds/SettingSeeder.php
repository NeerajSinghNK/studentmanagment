<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('website_setting')->updateOrInsert(['id'=>1],[ 
                                                        'name'=> 'logo',
                                                        'value'=> 'logo.in '
                                                    ]);
        DB::table('website_setting')->updateOrInsert(['id'=>2],[
                                                        'name'=> 'title',
                                                        'value'=> 'Title.in '
                                                    ]);
                                           
                                            
    }
}
