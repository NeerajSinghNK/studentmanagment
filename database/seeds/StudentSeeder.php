<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\Student;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$students = array (
		  0 => 
		  array (
		    'first_name' => 'Alok',
		    'last_name' => 'Choudhary',
		    'email' => 'alokchoudhary5134@gmail.com',
		    'phone' => '',
		    'batch' => '4',
		  ),
		  1 => 
		  array (
		    'first_name' => 'Dhruv',
		    'last_name' => 'rao',
		    'email' => 'dhruv07rao@gmail.com',
		    'phone' => '',
		    'batch' => '4',
		  ),
		  2 => 
		  array (
		    'first_name' => 'Krish',
		    'last_name' => 'Panwar',
		    'email' => 'pawarkrish242@gmail.com',
		    'phone' => '',
		    'batch' => '4',
		  ),
		  3 => 
		  array (
		    'first_name' => 'Manvi',
		    'last_name' => 'kadam',
		    'email' => 'kadam.deepti0111@gmail.com',
		    'phone' => '',
		    'batch' => '4',
		  ),
		  4 => 
		  array (
		    'first_name' => 'Surbhi',
		    'last_name' => 'upadhyay',
		    'email' => 'surbhiu952@gmail.com',
		    'phone' => '',
		    'batch' => '4',
		  ),
		  5 => 
		  array (
		    'first_name' => 'AARYA AGRAWAL',
		    'last_name' => 'AARYA',
		    'email' => 'aarya1610ag@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  6 => 
		  array (
		    'first_name' => 'Aditya kulkarni',
		    'last_name' => 'Aditya',
		    'email' => 'maheshkulkarni739@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  7 => 
		  array (
		    'first_name' => 'Akanksha jaiswal',
		    'last_name' => 'Akanksha',
		    'email' => 'akanksha230412@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  8 => 
		  array (
		    'first_name' => 'Akshat porwal',
		    'last_name' => 'Akshat',
		    'email' => 'mpakshat@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  9 => 
		  array (
		    'first_name' => 'Akshita behl',
		    'last_name' => 'Akshita',
		    'email' => 'akshitabehls@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  10 => 
		  array (
		    'first_name' => 'Ansh Jaiswal',
		    'last_name' => 'Ansh',
		    'email' => 'ansh998800@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  11 => 
		  array (
		    'first_name' => 'Arpit nayak',
		    'last_name' => 'Arpit',
		    'email' => 'arpitnayak147@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  12 => 
		  array (
		    'first_name' => 'Atharv Bhavsar',
		    'last_name' => 'Atharv',
		    'email' => 'aymbhavsar@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  13 => 
		  array (
		    'first_name' => 'Atharva Khandekar',
		    'last_name' => 'Atharva',
		    'email' => 'khandekaratharva7@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  14 => 
		  array (
		    'first_name' => 'Darshil Chitranshi',
		    'last_name' => 'Darshil',
		    'email' => 'darshilchitranshi2204@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  15 => 
		  array (
		    'first_name' => 'Harsh Gupta',
		    'last_name' => 'Harsh',
		    'email' => 'indore1974@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  16 => 
		  array (
		    'first_name' => 'Keyura Joshi',
		    'last_name' => 'Keyura',
		    'email' => 'mskeyurajoshi@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  17 => 
		  array (
		    'first_name' => 'Kirtan Dilip Makhija',
		    'last_name' => 'Kirtan',
		    'email' => 'kits0po9@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  18 => 
		  array (
		    'first_name' => 'Kunal patidar',
		    'last_name' => 'Kunal',
		    'email' => 'kunalpatidar99260@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  19 => 
		  array (
		    'first_name' => 'Maryam hussain',
		    'last_name' => 'Maryam',
		    'email' => 'maryam.rose.hussain@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  20 => 
		  array (
		    'first_name' => 'Muskan bansal',
		    'last_name' => 'Muskan',
		    'email' => 'muskanbansal99578@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  21 => 
		  array (
		    'first_name' => 'Nimisha Malleri',
		    'last_name' => 'Nimisha',
		    'email' => 'nimishamalleri@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  22 => 
		  array (
		    'first_name' => 'Nishchal Agrawal',
		    'last_name' => 'Nishchal',
		    'email' => 'agrawalnishchaln@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  23 => 
		  array (
		    'first_name' => 'Nishika Rohira',
		    'last_name' => 'Nishika',
		    'email' => 'nishikarohira5306@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  24 => 
		  array (
		    'first_name' => 'PRATEEK SOLANKI',
		    'last_name' => 'PRATEEK',
		    'email' => 'prateeksolanki312@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  25 => 
		  array (
		    'first_name' => 'Pravar Shukla',
		    'last_name' => 'Pravar',
		    'email' => '23pravarshukla@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  26 => 
		  array (
		    'first_name' => 'Raghav Purey',
		    'last_name' => 'Raghav',
		    'email' => 'kavitapurey444@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  27 => 
		  array (
		    'first_name' => 'Ujjwal fengde',
		    'last_name' => 'Ujjwal',
		    'email' => 'ujjfengde@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  28 => 
		  array (
		    'first_name' => 'Vishesh Dubey',
		    'last_name' => 'Vishesh',
		    'email' => 'dvishesh11@gmail.com',
		    'phone' => '',
		    'batch' => '1',
		  ),
		  29 => 
		  array (
		    'first_name' => 'Abhijeet Yadav',
		    'last_name' => 'Abhijeet ',
		    'email' => 'abhijeetyadav051@gmail.com',
		    'phone' => '62673 28973',
		    'batch' => '5',
		  ),
		  30 => 
		  array (
		    'first_name' => 'Akshat Desai',
		    'last_name' => 'Akshat Desai',
		    'email' => 'akshatdesai786@gmail.com',
		    'phone' => '99936 05093',
		    'batch' => '5',
		  ),
		  31 => 
		  array (
		    'first_name' => 'Amay Gupta',
		    'last_name' => 'Amay ',
		    'email' => 'amayguptag@gmail.com',
		    'phone' => '81030 14143',
		    'batch' => '5',
		  ),
		  32 => 
		  array (
		    'first_name' => 'Arnav Patil',
		    'last_name' => 'Arnav',
		    'email' => 'arnavpatil427@gmail.com',
		    'phone' => '98260 38696',
		    'batch' => '5',
		  ),
		  33 => 
		  array (
		    'first_name' => 'Ashwini Moghe',
		    'last_name' => 'Ashwini Moghe',
		    'email' => 'amoghe259@gmail.com',
		    'phone' => '88713 65526',
		    'batch' => '5',
		  ),
		  34 => 
		  array (
		    'first_name' => 'Bhavesh Lone',
		    'last_name' => 'Bhavesh ',
		    'email' => 'bhaveshlone@gmail.com',
		    'phone' => '62600 94510',
		    'batch' => '5',
		  ),
		  35 => 
		  array (
		    'first_name' => 'Devesh Danderwal',
		    'last_name' => 'Devesh Danderwal',
		    'email' => 'deveshdanderwal@gmail.com',
		    'phone' => '83193 66730',
		    'batch' => '5',
		  ),
		  36 => 
		  array (
		    'first_name' => 'Dewanshu Sharma',
		    'last_name' => 'Dewanshu Sharma',
		    'email' => 'dewanshusharma117@gmail.com',
		    'phone' => '70205 83338',
		    'batch' => '5',
		  ),
		  37 => 
		  array (
		    'first_name' => 'Durgesh Gothi',
		    'last_name' => 'Durgesh',
		    'email' => 'durgeshpatidat32@gmail.com',
		    'phone' => '62630 70917',
		    'batch' => '5',
		  ),
		  38 => 
		  array (
		    'first_name' => 'harsh dayaramani',
		    'last_name' => 'harsh dayaramani',
		    'email' => 'dayaramaniharsh16@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  39 => 
		  array (
		    'first_name' => 'Harsh Gharewal',
		    'last_name' => 'Harsh',
		    'email' => 'Namangharewal@gmail.com',
		    'phone' => '+91 70245 11313',
		    'batch' => '5',
		  ),
		  40 => 
		  array (
		    'first_name' => 'Heerak Agrawal',
		    'last_name' => 'Heerak Agrawal',
		    'email' => 'heeraksagrawal@gmail.com',
		    'phone' => '98275 29122',
		    'batch' => '5',
		  ),
		  41 => 
		  array (
		    'first_name' => 'isha joshi',
		    'last_name' => 'isha joshi',
		    'email' => 'isha.joshi709@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  42 => 
		  array (
		    'first_name' => 'ISHITA VISHWAKARMA',
		    'last_name' => 'ISHITA VISHWAKARMA ',
		    'email' => 'ishitavishwakarma43@gmail.com',
		    'phone' => '96171 96513',
		    'batch' => '5',
		  ),
		  43 => 
		  array (
		    'first_name' => 'Jaspreet kaur kamboj',
		    'last_name' => 'Jaspreet',
		    'email' => 'jaspreet6dec4@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  44 => 
		  array (
		    'first_name' => 'Jyoti Paswan',
		    'last_name' => 'Jyoti',
		    'email' => 'jyotipaswan332004@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  45 => 
		  array (
		    'first_name' => 'Jyotirjay Narayan Gupta',
		    'last_name' => 'Jyotirjay Narayan Gupta ',
		    'email' => 'jyotirjaygupta@gmail.com',
		    'phone' => '89898 05529',
		    'batch' => '5',
		  ),
		  46 => 
		  array (
		    'first_name' => 'Kathanika Mogeraya',
		    'last_name' => 'Kathanika Mogeraya',
		    'email' => 'mogerayak@gmail.com',
		    'phone' => '81205 94814',
		    'batch' => '5',
		  ),
		  47 => 
		  array (
		    'first_name' => 'Kaushal lokhande',
		    'last_name' => 'Kaushal lokhande',
		    'email' => 'kaushallokhande12345@gmail.com',
		    'phone' => '81033 13367',
		    'batch' => '5',
		  ),
		  48 => 
		  array (
		    'first_name' => 'KAUSHAL SINGH RAGHUWANSHI',
		    'last_name' => 'KAUSHAL',
		    'email' => 'kaushal31104@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  49 => 
		  array (
		    'first_name' => 'Kratika Vyas',
		    'last_name' => 'Kratika Vyas',
		    'email' => 'vyaskratika13@gmail.com',
		    'phone' => '93017 25032',
		    'batch' => '5',
		  ),
		  50 => 
		  array (
		    'first_name' => 'Parth Soni',
		    'last_name' => 'Parth',
		    'email' => 'soniparth078@gmail.com',
		    'phone' => '79997 02453',
		    'batch' => '5',
		  ),
		  51 => 
		  array (
		    'first_name' => 'piyush shakya',
		    'last_name' => 'piyush shakya',
		    'email' => 'raycoc12345@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  52 => 
		  array (
		    'first_name' => 'Rishi Joshi',
		    'last_name' => 'Rishi',
		    'email' => 'rishijoshi3107@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  53 => 
		  array (
		    'first_name' => 'Rishita lone',
		    'last_name' => 'Rishita',
		    'email' => 'rishitalone4@gmail.com',
		    'phone' => '',
		    'batch' => '5',
		  ),
		  54 => 
		  array (
		    'first_name' => 'Rishita vyas',
		    'last_name' => 'Rishita vyas',
		    'email' => 'rishitavyas720@gmail.com',
		    'phone' => '93407 49195',
		    'batch' => '5',
		  ),
		  55 => 
		  array (
		    'first_name' => 'Saif ansari',
		    'last_name' => 'Saif ansari',
		    'email' => 'saif4u8839@gmail.com',
		    'phone' => '88394 61431',
		    'batch' => '5',
		  ),
		  56 => 
		  array (
		    'first_name' => 'Shrey sharma',
		    'last_name' => 'Shrey sharma ',
		    'email' => 'sharmashrey6268@gmail.com',
		    'phone' => '62683 53343',
		    'batch' => '5',
		  ),
		  57 => 
		  array (
		    'first_name' => 'Suprit Anasane',
		    'last_name' => 'Suprit Anasane',
		    'email' => 'supritanasane1@gmail.com',
		    'phone' => '78985 02085',
		    'batch' => '5',
		  ),
		  58 => 
		  array (
		    'first_name' => 'Yash parate',
		    'last_name' => 'Yash parate',
		    'email' => 'yashparate31@gmail.com',
		    'phone' => '98934 56612',
		    'batch' => '5',
		  ),
		  59 => 
		  array (
		    'first_name' => 'ansh',
		    'last_name' => 'ansh singh panwar',
		    'email' => 'anshpanwar989@gmail.com',
		    'phone' => '95895 02000',
		    'batch' => '6',
		  ),
		  60 => 
		  array (
		    'first_name' => 'Deepika',
		    'last_name' => 'Deepika Mahodiya',
		    'email' => 'mahodiyad@gmail.com',
		    'phone' => '93015 40997',
		    'batch' => '6',
		  ),
		  61 => 
		  array (
		    'first_name' => 'Harshika',
		    'last_name' => 'Harshika Meena',
		    'email' => 'harshikameena2@gmail.com',
		    'phone' => '92029 05596',
		    'batch' => '6',
		  ),
		  62 => 
		  array (
		    'first_name' => 'Jayant',
		    'last_name' => 'Jayant Yadav',
		    'email' => 'yadavrakesh114@gmail.com',
		    'phone' => '78984 92901',
		    'batch' => '6',
		  ),
		  63 => 
		  array (
		    'first_name' => 'Kumkum',
		    'last_name' => 'Kumkum baswal ',
		    'email' => 'kumkumbaswal3752@gmail.com',
		    'phone' => '99930 66366',
		    'batch' => '6',
		  ),
		  64 => 
		  array (
		    'first_name' => 'Misbah ',
		    'last_name' => 'Misbah khan',
		    'email' => 'ak0300089@gmail.com',
		    'phone' => '93008 38130',
		    'batch' => '6',
		  ),
		  65 => 
		  array (
		    'first_name' => 'Tanmay',
		    'last_name' => 'Tanmay shukla',
		    'email' => 'tanmayshukla7477@gmail.com',
		    'phone' => '97547 95550',
		    'batch' => '6',
		  ),
		);

        foreach ($students as $key => $student) {
        	
        	$user  = array(
                    'first_name'   =>  $student['first_name'],
                    'last_name'    =>  $student['last_name'],
                    'email'        =>  $student['email'],
                    'role_id'      =>  3,
                    'password'     => Hash::make($student['first_name']."@1234"),
                    'image'		   => "demo.jpeg",
                    'status'	   => 1,
            );
        	$user = User::updateOrCreate(['email'=>$student['email']],$user);
        	$studentarray = array(
	        	'user_id'  => $user->id,          			
				// 'batch_id' => ,           			
        	);
        	$studentModal =  Student::updateOrInsert(
        									['user_id'	=>	$studentarray['user_id']],
        								 	['user_id'	=>	$studentarray['user_id']]
        								)->first();
        	// dd($student['batch'],$studentModal);
        	$studentModal->batch()->sync($student['batch']);
        }
    }
}
