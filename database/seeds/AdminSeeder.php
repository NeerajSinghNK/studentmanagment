<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->updateOrInsert([

            'role_id'      =>  '1'
        ],[
            'first_name'   =>  "Admin",
            'last_name'    =>  "Admin",
            'email'        =>  "admin@mailinator.com",
            'password'     => Hash::make('123456789'),
        ]);
    }
}
