<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

// Route::get('/command',function(){
//     Artisan::call('migrate');
// });


Route::middleware(['auth:web'])->group(function () {
	
	Route::prefix('admin')->group(function () {


		Route::middleware(['admin'])->group(function () {

			Route::get('/dashboard', 'AdminController@admindashboard')->name('admindashboard');

			Route::get('/updateProfile','AdminController@updateAdminProfile')
					 ->name('updateAdminProfile');
	   		Route::Post('/saveAdminProfile','AdminController@saveAdmin')
	   			  ->name('saveAdminProfile');

		    Route::get('/user/changestatus/{id}','Admin\StudentController@changeStatus')
				->name('changeStatus');

			Route::get('/batch/all', 'Admin\BatchController@viewAll')
				 ->name('batchviewall');
			Route::get('/batch/add', 'Admin\BatchController@create')
				 ->name('batchcreate');
			Route::post('/batch/save', 'Admin\BatchController@save')
				 ->name('batchsave');
			Route::get('/batch/update/{id}', 'Admin\BatchController@update')
				 ->name('batchupdate');
			Route::get('/batch/delete/{id}', 'Admin\BatchController@delete')
				 ->name('batchdelete');
			Route::get('/batch/index', 'Admin\BatchController@index')
				 ->name('batchindex');
			// Route::get('/batch/subjectWise/{subject}','Admin\BatchController@viewSubjectwiseStudent')
			// 	 ->name('studentviewSubjectwise');


			Route::get('/subject/all', 'Admin\SubjectController@viewAll')
				 ->name('subjectviewAll');
			Route::get('/subject/add', 'Admin\SubjectController@create')
				 ->name('subjectcreate');
			Route::post('/subject/save', 'Admin\SubjectController@save')
				 ->name('subjectsave');
			Route::get('/subject/update/{id}', 'Admin\SubjectController@update')
				 ->name('subjectupdate');
			Route::get('/subject/delete/{id}', 'Admin\SubjectController@delete')
				 ->name('subjectdelete');
                        Route::get('/chapter/all', 'Admin\ChapterController@viewall')
            	 		->name('chapterviewall');
			Route::get('/chapter/add', 'Admin\ChapterController@create')
				 ->name('chaptercreate');
			Route::post('/chapter/save', 'Admin\ChapterController@save')
				 ->name('chaptersave');
			Route::get('/chapter/update/{id}', 'Admin\ChapterController@update')
				 ->name('chapterupdate');
			Route::get('/chapter/delete/{id}', 'Admin\ChapterController@delete')
				 ->name('chapterdelete');

			
			Route::get('/teacher/all', 'Admin\TeacherController@viewAll')
				 ->name('teacherviewall');
			Route::get('/teacher/add', 'Admin\TeacherController@create')
				 ->name('teachercreate');
			Route::post('/teacher/save', 'Admin\TeacherController@save')
				 ->name('teachersave');
			Route::get('/teacher/update/{id}', 'Admin\TeacherController@update')
				 ->name('teacherupdate');
			Route::get('/teacher/delete/{id}', 'Admin\TeacherController@delete')
				 ->name('teacherdelete');
    		Route::get('/setting', 'AdminController@Setting')
    	 		->name('setting');
    		Route::get('/setting/upload/{id}','AdminController@SettingUpload')
    	 		->name('settingupload');
    		Route::post('/setting/save', 'AdminController@Settingsave')
    	 		->name('settingsave');
		});

		Route::middleware(['teacher'])->group(function () {

			Route::get('/updateProfile','AdminController@updateAdminProfile')
					 ->name('updateAdminProfile');
	   		Route::Post('/saveAdminProfile','AdminController@saveAdmin')
	   			  ->name('saveAdminProfile');

			Route::get('/setting', 'AdminController@Setting')
					 ->name('setting');
			Route::get('/setting/upload/{id}','AdminController@SettingUpload')
					 ->name('settingupload');
			Route::post('/setting/save', 'AdminController@Settingsave')
					 ->name('settingsave');

			Route::get('/teacher/all', 'Admin\TeacherController@viewAll')
					 ->name('teacherviewall');
			Route::get('/teacher/add', 'Admin\TeacherController@create')
					 ->name('teachercreate');
			Route::post('/teacher/save', 'Admin\TeacherController@save')
					 ->name('teachersave');
			Route::get('/teacher/update/{id}', 'Admin\TeacherController@update')
					 ->name('teacherupdate');
			Route::get('/teacher/delete/{id}', 'Admin\TeacherController@delete')
					 ->name('teacherdelete');

			Route::get('/batch/all', 'Admin\BatchController@viewAll')
				 ->name('batchviewall');
			Route::get('/batch/add', 'Admin\BatchController@create')
				 ->name('batchcreate');
			Route::post('/batch/save', 'Admin\BatchController@save')
				 ->name('batchsave');
			Route::get('/batch/update/{id}', 'Admin\BatchController@update')
				 ->name('batchupdate');
			Route::get('/batch/delete/{id}', 'Admin\BatchController@delete')
				 ->name('batchdelete');
			Route::get('/batch/index', 'Admin\BatchController@index')
				 ->name('batchindex');

			Route::get('/student/all', 'Admin\StudentController@viewAll')
				 ->name('studentviewAll');
			Route::get('/student/add', 'Admin\StudentController@create')
				 ->name('studentcreate');
			Route::post('/student/save', 'Admin\StudentController@save')
				 ->name('studentsave');
			Route::get('/student/update/{id}', 'Admin\StudentController@update')
				 ->name('studentupdate');
			Route::get('/student/delete/{id}', 'Admin\StudentController@delete')
				 ->name('studentdelete');
			Route::get('/student/batch/{batch}', 'Admin\StudentController@viewBatchwise')
				 ->name('studentviewbatchwise');
			
				 Route::get('/subject/all', 'Admin\SubjectController@viewAll')
				 ->name('subjectviewAll');
			Route::get('/subject/add', 'Admin\SubjectController@create')
				 ->name('subjectcreate');
			Route::post('/subject/save', 'Admin\SubjectController@save')
				 ->name('subjectsave');
			Route::get('/subject/update/{id}', 'Admin\SubjectController@update')
				 ->name('subjectupdate');
			Route::get('/subject/delete/{id}', 'Admin\SubjectController@delete')
				 ->name('subjectdelete');


            Route::get('/chapter/all', 'Admin\ChapterController@viewall')
     	 		->name('chapterviewall');
			Route::get('/chapter/add', 'Admin\ChapterController@create')
				 ->name('chaptercreate');
			Route::post('/chapter/save', 'Admin\ChapterController@save')
				 ->name('chaptersave');
			Route::get('/chapter/update/{id}', 'Admin\ChapterController@update')
				 ->name('chapterupdate');
			Route::get('/chapter/delete/{id}', 'Admin\ChapterController@delete')
				 ->name('chapterdelete');
			Route::get('/chapter/subject/{id}', 'Admin\ChapterController@subjectWise')
				 ->name('chaptersubjectWise');

			Route::get('/notification/all', 'Admin\NotificationController@viewAll')
				 ->name('notificationviewall');
			Route::get('/notification/batcht/{batch}', 'Admin\NotificationController@batchtWise')
				 ->name('notificationbatchtWise');
			Route::get('/notification/add', 'Admin\NotificationController@create')
				 ->name('notificationcreate');
			Route::post('/notification/save', 'Admin\NotificationController@save')
				 ->name('notificationsave');
			Route::get('/notification/update/{id}', 'Admin\NotificationController@update')
				 ->name('notificationupdate');
			Route::get('/notification/delete/{id}', 'Admin\NotificationController@delete')
				 ->name('notificationdelete');


			Route::get('/link/all', 'Admin\LinkController@viewAll')
				 ->name('linkviewall');
			Route::post('/link/all', 'Admin\LinkController@viewAll');
			//Route::get('//batcht/{batch}', 'Admin\NotificationController@batchtWise')
				 // ->name('notificationbatchtWise');
			Route::get('/link/add', 'Admin\LinkController@create')
				 ->name('linkcreate');
			Route::post('/link/save', 'Admin\LinkController@save')
				 ->name('linksave');
			Route::get('/link/update/{id}', 'Admin\LinkController@update')
				 ->name('linkupdate');
			Route::get('/link/delete/{id}', 'Admin\LinkController@delete')
				 ->name('linkdelete');

			Route::get('/link/dynamicChapter/{id}', 'Admin\LinkController@getChapter')
				 ->name('dynamicChapter');
			// Route::get('/link/index', 'Admin\LinkController@index')
				 // ->name('linkindex');

			Route::post('/link/getpreviouslink/', 'Admin\LinkController@getPreviousLink')
				 ->name('getpreviouslink');

		});
	});

	Route::prefix('student')->group(function () {
		Route::middleware(['student'])->group(function () {

			Route::get('/dashboard','Student\StudentController@dashboard')
				 ->name('studentDashboard'); 
			
			Route::get('/showAll','Student\StudentController@showAllNotification')
				 ->name('studentAllNotification');
			Route::get('/previousLectureSubject','Student\StudentController@showAllSubject')
				 ->name('studentAllSubject'); 
			Route::get('/pdfMaterial','Student\StudentController@showPdfMaterial')
				 ->name('studentPdfMaterial');
			Route::get('/allChapter/{id}','Student\StudentController@showPreviousSubject')
				 ->name('previousSubjectLink');
			Route::get('/previousSubject/{id}','Student\StudentController@showPreviousSubject')
				 ->name('previousSubjectLink');
			Route::get('/showAllChapter','Student\StudentController@getAllChapter')
				 ->name('allChapter');
			Route::get('/zoomClassLink','Admin\LinkController@showZoomLink')
				 ->name('studentZoomLink');
			Route::get('/showLink/{id}','Student\StudentController@showAllLink')
				->name('ShowAllChapterLink');
			Route::post('/postLink','Admin\LinkController@postLink')
				 ->name('studentPostLink');
		   	
			Route::get('/theoryMaterial','Student\StudentController@showTheoryMaterial')
				 ->name('studentTheoryMaterial');
		   	Route::get('/theoryMaterial/{subjectId}','Student\StudentController@theoryMaterialSubject')
				 ->name('subjectTheoryMaterial');

			Route::get('/excersiseMaterial','Student\StudentController@showExcersiseMaterial')
				 ->name('studentExcersiseMaterial');
			Route::get('/excersiseMaterial/{id}','Student\StudentController@excersiseMaterialSubject')
				 ->name('subjectExcersiseMaterial');

			Route::get('/updateProfile','Student\StudentController@updateUserProfile')
					 ->name('updateStudentProfile');
	   		Route::Post('/saveProfile','Student\StudentController@saveUser')
					 ->name('saveStudentProfile');
					 
			Route::get('/showTodaySubject','Student\StudentController@showTodayLecture')
				->name('showTodayLeactureSubjects');
			Route::get('/showTodayLectureChapter/{id}', 'Student\StudentController@showTodayChapters')
				->name('showTodayChapter');
			Route::get('/showTodayLink/{id}','Student\StudentController@todayClassLink')
				->name('TodayClassLink');
			Route::get('/showTheoryChapter/{id}','Student\StudentController@showTheoryChapter')
				->name('theoryChapter');
			Route::get('/showExcersieChapter/{id}','Student\StudentController@showExcersiseChapter')
				->name('excersiseChapter');
				

		});

	}); 
});


