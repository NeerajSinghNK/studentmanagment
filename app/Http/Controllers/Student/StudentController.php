<?php

namespace App\Http\Controllers\Student;


use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Subject;
use App\Models\Batch;
use App\Models\Link;
use App\Models\Student;
use App\Models\Chapter;
use App\Helpers\Dashboardhelper;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {   
        // $preSubject = $this->showPreviousSubject();
        $links = new Link;
        $links = $links->getAll();
        $subject = new Subject;
        $chapter = new Chapter;
       return view('student/dashboard', compact('links','subject','chapter'));
    }

   

    public function saveUser(Request $request ){ 
        
        $this->validate($request,[
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:255'],
            'email'         => ['required', 'string', 'email', 'max:255'],
            'password'      => ['nullable', 'string', 'min:8', 'confirmed'],
            
        ]);
        
        $user = ['first_name'=> $request->first_name,'last_name' => $request->last_name];

        if(!empty($request->password)){
            $user['password'] = Hash::make($request->password);

        }
          if($request->hasFile('image')){ 
           $image =  $request->image->store('upload/profilepic','public');
           $user['image'] = $image;
        }
        Auth::user()->update($user);
        
        return redirect()->route('updateStudentProfile');
    }


    public function updateUserProfile(){
        return view('student/studentProfile');
    }


    public function showAllNotification()
    {
        $notifications =  new Notification;
        $notifications = $notifications->viewAllNotification();
        
        return view('student/allnotification',compact('notifications'));
    }

    public function showAllSubject()
    {
        $user = auth()->user()->student;
        $subjects = $user->getAllSubject();
        $chapter = new Chapter;
        return view('student/previousLectureSubject',compact('subjects','chapter'));
    }

    public function showTodayLecture(){

        $subjects = auth()->user()->student->getAllSubject();
        return view('student/showTodaySubject',compact('subjects'));
    }
//This function shows the today class links
    public function showTodayChapters($subject_id)
    {
        $Links    = new Link;
        $Links    = $Links->getlinkBySubject($subject_id,'Class');  
        return view('student/showlink',compact('Links'));
    }
//end function
    public function showTheoryChapter($subject_id){
        $chapters = Subject::find($subject_id)->chapters; 
        $subject = new Subject; 
        return view('student/showTheoryChapter',compact('chapters','subject'));
    }

    public function showExcersiseChapter($subject_id){
        $chapters = Subject::find($subject_id)->chapters;  
        $subject = new Subject;
        return view('student/showExcersiseChapter',compact('chapters','subject'));
    }

    // The below function will get the chapter for previous subject...
    public function showPreviousSubject($subject_id)
    {
        $chapters = Subject::find($subject_id)->chapters;
        $subject = new Subject;
        return view('student/showAllChapter',compact('chapters','subject'));
    }
    // end Previous chapter function

    public function showPdfMaterial()
    {
        $subject = new Subject;
        $chapter = new Chapter;
        return view('student/pdfmaterial',compact('subject','chapter'));
    }

    public function showExcersiseMaterial()
    {
        $subject = new Subject;
        $chapter = new Chapter;
        return view('student/excersise',compact('subject','chapter'));
    }

    public function showTheoryMaterial()
    {
        $subject = new Subject;
        $chapter = new Chapter;
        return view('student/theory',compact('subject','chapter'));
    }


    public function showAllLink($chapter_id){
        $Links    = new Link;
        $subject  = new Subject;
        $Links    = $Links->getlinkByChapter($chapter_id,'Previous');  
        return view('student/showlink',compact('Links','subject'));
    }

    // public function todayClassLink($chapter_id){
    //     $Links    = new Link;
    //     $subject = new Subject;
    //     $Links    = $Links->getlinkByChapter($chapter_id,'Class',$subject->id);  
    //     return view('student/showlink',compact('Links','subject'));
    // }

    // This function will get the theory link using chapter id
    public function theoryMaterialSubject($chapter_id)
    {
        $Links    = new Link;
        $subject  = new Subject;
        $Links    = $Links->getlinkByChapter($chapter_id,'Theory');
        return view('student/showlink',compact('Links','subject'));
    }
// end of theory link....

    // this function will return the excersise link using chapter id
    public function excersiseMaterialSubject($chapter_id)
    {
        $Links    = new Link;
        $subject = new Subject;
        $Links    = $Links->getlinkByChapter($chapter_id,'Excersise');
        return view('student/showlink',compact('Links','subject'));
    }
    // end of excersie link



}
