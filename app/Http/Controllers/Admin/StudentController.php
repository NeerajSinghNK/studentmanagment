<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Student;
use App\Models\Batch;
use Auth;
use DB;
class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewAll(){

        $students =  new Student;
        $students = $students->getAll();
        $batches  = Batch::all();
        return view('admin.student.listall',compact('students','batches'));
    }
 
    public function create(){  

        $student = new Student;
        $student->user = new User;
       
        $batches = new Batch;
        $batches = $batches->getAll();
        return view('admin.student.create',compact('student','batches'));
    }

    public function update($id){  
    
        $student = Student::with('user')->findOrFail($id);

        $batches = new Batch;
        $batches = $batches->getAll();
        
        return view('admin.student.student',compact('student','batches'));
    }

    public function save(Request $request){ 
        
        $this->validate($request,[
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:255'],
            'email'         => ['required', 
                                'string', 
                                'email', 
                                'max:255', 
                                'unique:users,email,'.$request->user_id
                                ],
            'password'      => ['nullable', 'string', 'min:8', 'confirmed'],
            'batch_ids'     => 'required|array',
            // 'state'         => ['required','string','max:255'],
            // 'city'          => ['required','string','max:255'],
            // 'pincode'       => ['required','string','max:6'],
            // 'village'       => ['required','string','max:255'],
            // 'road'          => ['required','string','max:255'],
            // 'colony'        => ['required','string','max:255'],
            // 'house_no'      => ['required','string','max:255'],
            // 'landmark'      => ['required','string','max:255'],
        ]);
          
        if(isset($request->image) && $request->hasFile('image')){
            $profilepic = $request->image->store('upload/profilepic','public');
        }else{
            $profilepic = $request->profile_pic;
        }

        if(!is_null($request->radiobutton)){
            $status= $request->radiobutton;
        }
           
           $studentarray=array(
                'first_name'    => $request->first_name,           
                'last_name'     => $request->last_name,
                'email'         => $request->email,
                'password'      => $request->password,
                'user_id'       => $request->user_id,
                'batch_ids'     => $request->batch_ids,
                'image'         => $profilepic,
                'status'        => $status,
                'country'       => $request->country,
                'state'         =>  $request->state,
                'city'          =>  $request->city,
                'pincode'       =>  $request->pincode,
                'village'       =>  $request->village,
                'road'          =>  $request->road,
                'colony'        =>  $request->colony,
                'house_no'      =>  $request->house_no,     
                'landmark'      =>  $request->landmark,
                'contact'       =>  $request->contact,
           );
         // dd($request->all());
        $student = Student::findOrNew($request->id);
        $student->saveStudent($studentarray);
        
        return redirect()->route('studentviewAll');
    }

    public function delete($id){

        $student = Student::find($id)->delete();
        return redirect()->route('studentviewAll');
    }
    
    public function viewBatchwise($batchId){ 

        $students = Student::with('user','batch')
                            ->whereHas('batch', function($q) use($batchId){
                                $q->where('batch_id','=', $batchId);
                            })
                            ->get();
        $batches  = Batch::all();
        return view('admin.student.listall',compact('students','batches'));
    }


    public function changeStatus(Request $request,$id)
    {
        try{ 
            $user = User::find($id);
            $user->changeStatus($request->status);
            $response = array(
                "success" => true,
                "type"    => "success",
                "message" => "Status change successfully",
            );
        }catch( \Exception $e) { 
            $response = array(
                "success"       => false,
                "type"          => "error", 
                "message"       => "something went wrong",
                "description"   => $e->getMessage(),
            );
        }
        return response()->json($response);
    }
}


