<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Teacher;
use App\Models\Subject;
use DB;
class TeacherController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewAll(){

        $teachers =  new Teacher;
        $teachers = $teachers->getAll();
        return view('admin.teacher.listall',compact('teachers'));
    }

    public function create(){  

        $teacher = new Teacher;
        $teacher->user = new User;
        // $teacher = user->getAll();
        $subjects  = new Subject;
        $Allsubjects = $subjects->getAll();
        return view('admin.teacher.create',compact('teacher','Allsubjects'));
    }

    public function update($id){  
    
        $teacher     = Teacher::with('user')->findOrFail($id);
        $subjects    = new Subject;
        $Allsubjects = $subjects->getAll();
        return view('admin.teacher.teacher',compact('teacher','Allsubjects'));
    }

    public function save(Request $request){ 
        
        $this->validate($request,[
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            'email'      => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.$request->user_id],
            'password'      => ['nullable', 'string', 'min:8', 'confirmed'],
            'subjects'      => 'required|array',
            // 'state'         => ['required','string','max:255'],
            // 'city'          => ['required','string','max:255'],
            // 'pincode'       => ['required','string','max:6'],
            // 'village'       => ['required','string','max:255'],
            // 'road'          => ['required','string','max:255'],
            // 'colony'        => ['required','string','max:255'],
            // 'house_no'      => ['required','string','max:255'],
            // 'landmark'      => ['required','string','max:255'],

        ]);

        if(isset($request->image) && $request->hasFile('image')){
            $profilepic = $request->image->store('upload/profilepic','public');
        }else{
            $profilepic = $request->profile_pic;
        }
 
        if(!is_null($request->radiobutton)){
            $status= $request->radiobutton;
        }
           
           $Teacherarray = array(
                'first_name'    => $request->first_name,           
                'last_name'     => $request->last_name,
                'email'         => $request->email,
                'password'      => $request->password,
                'subjects'      => $request->subjects,
                'user_id'       => $request->user_id,
                'batch_id'      => $request->batch_id,
                'image'         => $profilepic,
                'status'        => $status,
                'country'       => $request->country,
                'state'         => $request->state,
                'city'          => $request->city,
                'pincode'       => $request->pincode,
                'village'       => $request->village,
                'road'          => $request->road,
                'colony'        => $request->colony,
                'house_no'      => $request->house_no,     
                'landmark'      => $request->landmark,
                'contact'       => $request->contact, 
           );
        
            //  dd($request->all());
        $Teacher = Teacher::findOrNew($request->id);
        $Teacher->saveTeacher($Teacherarray);
        return redirect()->route('teacherviewall');
    }

    public function delete($id){

        $Teacher = Teacher::find($id)->delete();
        return redirect()->route('teacherviewall');
    }
    
    

}


