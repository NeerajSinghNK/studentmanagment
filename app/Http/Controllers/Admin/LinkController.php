<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Link;
use App\Models\Batch;
use App\Models\Subject;
use App\Models\Chapter;
use Session;
use App\Http\Requests\LinkstoreRequest;

class LinkController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    // public function index(){
    //     $data = Link::find(1)->batch->toArray();
    //     echo'<pre>';
    //     print_r($data);
    //     echo'</pre>';
        
    // } 


    public function viewAll(Request $request){

        if($request->method() == "POST"){
            $search = array(
                'types'    => is_array($request->types)? $request->types : array(),                    
                'chapters' => is_array($request->chapters)? $request->chapters : array(),                        
                'subjects' => is_array($request->subjects)? $request->subjects : array(),
                'batchs'   => is_array($request->batchs)? $request->subjects : array(),
            );
            Session::put('search',$search);
        }else{
            $search = array(
                'types'    => is_array(Session::get('search.types')) ? Session::get('search.types') : array(),                    
                'chapters' => is_array(Session::get('search.chapters')) ? Session::get('search.chapters') : array(),                        
                'subjects' => is_array(Session::get('search.subjects')) ? Session::get('search.subjects') : array(),
                'batchs'   => is_array(Session::get('search.batchs')) ? Session::get('search.batchs') : array(),      
            );
        
        }
        // dd($search);
        $links =  new Link;
        $links = $links->getAll($search);
        return view('admin.link.listall',compact('links','subjectId','chapterId','search'));
    }

    public function create(){  

        $subjects  = new Subject;
        $subjects = $subjects->getAll();
        
        $batchs = new Batch;
        $batchs = $batchs->getAll();
        
        $link = new Link;
        return view('admin.link.link',compact('link', 'subjects', 'batchs'));
    }


 
    public function update($id){  
        
        $subjects  = new Subject;
        $subjects = $subjects->getAll();
        
        $batchs = new Batch;
        $batchs = $batchs->getAll();
        
        // $chapters = new Chapter;
        // $chapters = $chapters->getAll();
        
        $link = link::findOrFail($id);
        return view('admin.link.link',compact('link', 'subjects','batchs'));
    }


    public function postLink(Request $request) 
    {   
        $ids=$request->get('postLink');
        // dd($ids);
        $link = Link::where('id', $ids)
                    ->get('link');
        // dd($link['0']['link']);
        return redirect()->away($link['0']['link']);

     }


    // public function showZoomLink()
    // {
    //     $link = new Link;
    //     $link = $link->getClassLink();
    //     // dd($link);
    //     return view('student/zoomClassLink',compact('link'));
    // }


    public function save(LinkstoreRequest $request){ 
        
        $link= new Link;
        $link->saveLink($request->all());
        return redirect('admin/link/all');
    }

    public function delete($id){

        $link = Link::find($id)->delete();
        return redirect('admin/link/all');
    }

    public function getChapter($id)
    {
        $chapters = new Chapter;
        $chapters = $chapters->getChapterBySubjectId($id);
        return  view('admin.link.partial.chapterOption', compact('chapters'))->render();
    }

    public function getPreviousLink(Request $request)
    {

        $links  =   Link::join('batch_link','batch_link.link_id','links.id')
                        ->when(!empty($request->subject_id), function ($query) use($request){

                            return $query->where('subject_id',$request->subject_id);
                        })
                        ->when(!empty($request->chapter_id), function ($query) use($request){

                            return $query->where('chapter_id',$request->chapter_id);
                        })
                        ->when(!empty($request->title), function ($query) use($request) {
                            
                            return $query->Where('title','like',"%".$request->title."%");
                        })
                        ->when(!empty($request->description), function ($query) use($request)
                        {
                            return $query->Where('description','like',"%".$request->description."%");
                        })
                        ->when(!empty($request->batch_id) && count($request->batch_id) > 0, function ($query) use($request) 
                        {
                            return $query->WhereIn('batch_link.batch_id',$request->batch_id);
                        })
                        ->when(empty($request->subject_id), function ($query){

                            return $query->where('links.id',null);
                        })
                        ->groupBy('batch_link.link_id')
                        ->select('links.*')
                        ->get();

        return  view('admin.link.partial.previousLink', compact('links'))->render();

    }

}