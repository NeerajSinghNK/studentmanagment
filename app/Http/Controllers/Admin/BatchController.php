<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Batch;
use App\Models\Subject;

class BatchController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $data = Batch::find(1)->subjects->toArray();
        echo'<pre>';
        print_r($data);
        echo'</pre>';
        
    }
    public function viewAll(){

        $batchs =  Batch::all();
        return view('admin.batch.listall',compact('batchs'));
    }

    public function create(){  

        $batch    = new Batch;
        $subjects = Subject::all();
        return view('admin.batch.batch',compact('batch','subjects'));
    }

    public function update($id){  
    
        $batch    = Batch::findOrFail($id);
        $subjects = Subject::all();
        return view('admin.batch.batch',compact('batch','subjects'));
    }

    public function save(Request $request){         
        
        $this->validate($request,[
            'batch_name'       => 'required|max:255|unique:batch,batch_name,'.$request->id,
            'class'            => 'required|max:255',
            'subject_ids'      => 'required|array',
            // 'stream'           => 'required|max:255',
            // 'batch_start_date' => 'required|date',
            // 'batch_end_date'   => 'required|date', 
        ]);

      $Batch = new Batch;
      $Batch->saveBatch($request->all());

      return redirect()->route('batchviewall');
    }

    public function delete($id){

        $Batch = Batch::find($id)->delete();
        return redirect()->route('batchviewall');
    }

    // public function viewSubjectwiseStudent($subjectId){
    //     $batch = Batch::with('subject','batch')
    //                         ->where('id',$subjectId)
    //                         ->get();
    //     $subjects  = Subject::all();
    //     return view('admin.batch.listall',compact('batch','subjects'));

    // }

}


