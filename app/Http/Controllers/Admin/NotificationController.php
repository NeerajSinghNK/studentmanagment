<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Notification;
use App\Models\Batch;

class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function viewAll(){

        $notifications =  new Notification;
        $notifications = $notifications->getAll();
        
        return view('admin.notification.listall',compact('notifications'));
    }

    public function batchtWise($batchId){

        $notifications =  new Notification;
        $notifications = $notifications->all();
        return view('admin.notification.listall',compact('notifications'));
    }

    public function create(){  

        $notification       = new Notification;
        $batches = new Batch;
        $batches = $batches->getAll();
        return view('admin.notification.create',compact('notification','batches'));
    }

    public function update($id){  
    
        $notification = Notification::with('notification')->findOrFail($id);
        $batches = new Batch;
        $batches = $batches->getAll();
        return view('admin.notification.notification',compact('notification','batches'));
    }

    public function save(Request $request){ 
        //dd($request->all());
        $this->validate($request,[
            'message'  => 'required|string|max:255',
            'batch_id' => 'required|string|max:255',
            'radiobutton'   => 'string',
        ]);
      
        $notification  = new Notification;
        // $notification = $request->input('radiobutton');
        $notification->saveNotification($request->all());
        //   dd($request->all());

        //$notification->save();
        return redirect('admin/notification/all');
    }

    public function delete($id){

        $notification = Notification::find($id)->delete();
        return redirect('admin/notification/all');
    }

}
