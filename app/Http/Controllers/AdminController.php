<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Setting;
use Illuminate\Support\Facades\Storage;
use Auth;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function admindashboard(){  
        $student = new Student;
        $teacher = new Teacher;
        $student = $student->getStudentData();
        // $demostudent = $student->getDemoStudentData();
        $teacher = $teacher->getTeacherData();
        
        return view('admin.dashboard',compact('student','teacher'));
    } 
    public function Setting(){
       
        $website_setting =  new Setting;
        $website_setting = $website_setting->getAll();
        return view('admin.setting.listall', compact('website_setting'));
    }
    public function SettingUpload(){
         
        $website_setting = Setting::all();
        foreach ($website_setting as $key => $set) {
            $settings[$set->name] = $set->value;
            
        }
         

        return view('admin.setting.setting', compact('settings'));
    }
     public function Settingsave(Request $request){ 

        $this->validate($request,[
            'title'      => 'required|string|max:255',
            'logofile'   => 'nullable|mimes:png,jpg,jpeg,gif',
            'logo'       => 'required',
        ]);

        if(!is_null($request->logofile)){
            $logo = $request->logofile->store('upload/logos','public');
        }else{
            $logo = $request->logo;
        }
        $setting = array(
            'title' => $request->title,
            'logo'  => $logo, 
        );
        $website_setting= new Setting;
        $website_setting->saveSetting($setting);
         
        return redirect('admin/setting');
    }



    public function updateAdminProfile(){

        return view('admin/adminProfile/adminProfileData');
    }

    public function saveAdmin(Request $request){

        $this->validate($request,[
            'first_name'    => ['required', 'string', 'max:255'],
            'last_name'     => ['required', 'string', 'max:255'],
            'email'         => ['required', 'string', 'email', 'max:255'],
            'password'      => ['nullable', 'string', 'min:8', 'confirmed'],
            // 'state'         => ['required','string','max:255'],
            // 'city'          => ['required','string','max:255'],
            // 'pincode'       => ['required','string','max:6'],
            // 'village'       => ['required','string','max:255'],
            // 'road'          => ['required','string','max:255'],
            // 'colony'        => ['required','string','max:255'],
            // 'house_no'      => ['required','string','max:255'],
            // 'landmark'      => ['required','string','max:255'],
            
        ]);
        // dd($request->all()); 
        
        $user = [
            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'country'    => $request->country,
            'state'      => $request->state,
            'city'       => $request->city,
            'pincode'    => $request->pincode,
            'village'    => $request->village,
            'road'       => $request->road,
            'colony'     => $request->colony, 
            'house_no'   => $request->house_no,
            'landmark'   => $request->landmark,
            'contact'    => $request->contact,
        ];
        // dd($user);
        if(!empty($request->password)){
            $user['password'] = Hash::make($request->password);
        }
        // dd($user);   
        if($request->hasFile('image')){
               
           $image =  $request->image->store('upload/profilepic','public');
           $user['image'] = $image;
        }
        
        Auth::user()->update($user);       
        return redirect('admin/dashboard');

    }


}


