<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChapterStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
        {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'chap_name'  => 'required|string|max:255',
            'chap_type'  => 'required|string|max:255',
            'chap_class' => 'required|array',
            'subject_id' =>  'required',
            // 'class'      =>  'required',
            // 'priority'   => 'required|numeric|min:1|max:100|unique:chapters,priority,'.$request->id.',id,subject_id,'.$request->subject_id.',chap_class,'.$request->chap_class,
            'priority'   => Rule::unique('chapters','priority')
                            ->ignore($this->id)
                            ->where('subject_id',$this->subject_id)
                            // ->whereIn('chap_class',$this->chap_class)
                            ->where(function($query) {
                                foreach($this->chap_class as $class){
                                $query->orWhere('chap_class', 'like', "%".$class."%");
                                }        
                             })
        ];
    }
}
