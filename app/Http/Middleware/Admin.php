<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {    
//        if (!Auth::check()) {
//       
//            return redirect()->route('login');
//        }
//            
//        if (Auth::user()->role == 1) {
//            return redirect()->route('admin/admindashboard');
//        }
//
//        if (Auth::user()->role == 2) {
//            return redirect()->route('studentviewAll');
//        }
//     if (Auth::user()->role == 3) {
//            return $next($request);
//        }
        if(Auth::user()->role_id == 1){
             
            return $next($request);
        }
        if(Auth::user()->role_id == 2)
        {  
            Auth::user();
            return redirect()->route('studentviewAll');
        }
     else{
            return redirect()->back();
        }
    }
}
