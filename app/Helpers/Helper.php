<?php

// namespace App\Helpers;
// use App\Http\Controllers\NotificationController;
use App\Http\Controllers\Student\NotificationController;
use App\Models\Setting;
use App\Models\Link;
// use App\Http\Middleware\Student;

// class Helper{

   
	function getLatestNotification()
	{
	    $NotificationController = new NotificationController;
	    // dd($NotificationController);
	    return $NotificationController->index();
	}
    
	function getLogo()
	{
	    $website_setting =  new Setting;
	    $website_setting = $website_setting->getLogo();
	    return url('storage/'.$website_setting->value);
	}

	function getLinkCountByType($type,$subject_id=null,$chapter_id=null){
		$Link = new Link;
		// =
		$type = explode(',',$type);
		return  $Link->countAllTypeOfLink($type, $subject_id, $chapter_id);
	}
// }
?>