<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Student;
use App\Models\Teacher;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'token',
        'role_id',
        'image',
        'status',
        'country',
        'state',
        'city',
        'pincode',
        'village',
        'road',
        'colony',
        'house_no',
        'landmark',
        'contact' 
    ]; 

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function changeStatus($status)
    {
        $this->status = $status;
        $this->save();
    }

    public function student()
    {
        return $this->hasOne('App\Models\Student');
    }

    public function teacher()
    {
        return $this->hasOne('App\Models\Teacher');
    }

    public function link()
    {
        return $this->hasOne('App\Models\Link');
    }


    public function fullname()
    {
        return $this->first_name.' '.$this->last_name;
    }

}
