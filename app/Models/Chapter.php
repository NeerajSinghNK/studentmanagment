<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
Use App\User;
use Hash;
use \DB;
class Chapter extends Model
{
	protected $fillable = [
        'chap_name',
		'chap_type',
		'chap_class',
		'priority',
		'subject_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'priority' => 'numeric',
    ];

    public function saveChapter($chapter){  
        $chapter['chap_class'] = implode(',', $chapter['chap_class']);
        $user = $this->updateOrCreate(['id' => $chapter['id']] ,$chapter);
    }
    // public function chapter(){
    //     return $this->hasOne('App\Models\Chapter', 'id', 'subject_id');
    // }

    public function getAll()
    {
    	return $this->with('subject')->get();
    } 
    
    public function subject()
    {
        return $this->hasOne('App\Models\Subject', 'id', 'subject_id');
    }
    public function links()
    {
        return $this->hasMany('App\Models\Link', 'chapter_id');
    }
     
    public function getChapterBySubjectId($subject_id)
    {
        $chapter = Chapter::where("subject_id",$subject_id)
                        ->get();
        return $chapter;
       
    }
}
