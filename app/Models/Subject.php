<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
Use App\User;
use Hash;
use App\Models\Link;
class Subject extends Model
{
	protected $fillable = [
        'name',
		'subject_category',
    ];
    
    
    public function getAll()
    {
    	return $this->all();
    }

    
    
    
     public function saveSubject($subject){  

         $user = $this->updateOrCreate(['id' => $subject['id']] ,$subject);
        
     }                 
        
    


    //   public function user()
    // {
    //     return $this->hasOne('App\User', 'id', 'id');
    // }

    // public function batchSubject()
    // {
    //     return $this->hasOne('App\', 'id', 'id');
    // }
    public function links(){
        return $this->belongsToMany('App\Models\Link', 'link_id', 'subject_id');
    }

    public function batchs(){
        return $this->belongsToMany('App\Models\Batch')
                    ->withPivot('batch_id','subject_id');
                    // ->withTimestamp();
    }


    public function chapters()
    {
//        return $this->belongsToMany(Batch::class,'batch_subject', 'subject_id','batch_id');
        return $this->hasMany('App\Models\Chapter', 'subject_id');
    }


}
