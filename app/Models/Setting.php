<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
Use App\User;
use Hash;
class Setting extends Model
{
	 protected  $table = 'website_setting';

      protected $fillable = [
        'name',
		'value',
		
    ];

    public function saveSetting($website_setting){  
        foreach ($website_setting as $key => $value) {
             $this->updateOrCreate(['name' => $key ],['value'=>$value]) ;
        }
       
    }

    public function getAll()
    {
    	return $this->all();
    }
    
    public function getLogo()
    {
        return $this->where('name','logo')->first();
    }
	 
}
