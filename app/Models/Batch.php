<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
	protected $table="batch";

	protected $fillable = [
        'batch_name',
		'class',
		'language',
		'stream',
		'remarks',
		'batch_start_date',
		'batch_end_date',
    ];

    public function getAll()
    { 
   	return $this->with('subjects')->get();
    }
      public function subject()
    {
        return $this->hasOne('App\Models\Subject', 'id', 'subject_id');
    }

    public function saveBatch($batchData){
    	$batch = $this->updateOrCreate(['id' => $batchData['id']] ,$batchData);	
        $batch->subjects()->sync($batchData['subject_ids']);
    }
   
    

    public function subjects() 
    {
//        return $this->belongsToMany(Subject::class,'batch_subject', 'subject_id','batch_id');
        return $this->belongsToMany('App\Models\Subject')
                    ->withPivot('batch_id' ,'subject_id')
                     ->withTimestamps();
    }
    
    public function links()
    {
        return $this->hasOne('App\Models\Link','id', 'link_id');
    }
}
