<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use App\User;
use DB;

class Teacher extends Model
{  
    protected $table = "teachers";
	public $roleId      = 2;
	protected $fillable = [  
        'user_id', 
    ];

    public function saveTeacher($Teacher){  
        
        DB::beginTransaction();
        try {
        
            $user  = array(
                        'first_name'   =>  $Teacher['first_name'],
                        'last_name'    =>  $Teacher['last_name'],
                        'email'        =>  $Teacher['email'],
                        'role_id'      =>  $this->roleId,
                        'image'        =>  $Teacher['image'],
                        'status'       =>  $Teacher['status'],
                        
                        'state'        =>  $Teacher['state'],
                        'city'         =>  $Teacher['city'],
                        'pincode'      =>  $Teacher['pincode'],
                        'village'      =>  $Teacher['village'],
                        'road'         =>  $Teacher['road'],
                        'colony'       =>  $Teacher['colony'],
                        'house_no'     =>  $Teacher['house_no'],     
                        'landmark'     =>  $Teacher['landmark'], 
                        'country'      =>  $Teacher['country'],
                        'contact'      =>  $Teacher['contact'],

                    ); 
            
            if(!empty($user['password'])){
                $user['password'] = Hash::make($Teacher['password']);
            }
            $user = User::updateOrCreate(['id' => $Teacher['user_id']] ,$user);
            // dd($this->subjects()->sync($Teacher['subject']));
            $this->user_id=$user->id;
            $this->save();
            $this->subjects()
                ->sync($Teacher['subjects']); 
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            dd($e);
        }
    }

    public function getAll(){  
        
        return $this->with('user')->get();
    }

    public function getTeacherData(){
        $user = auth()->user();
        $Teacher = user::where('role_id',2);
        return $Teacher;
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function subjects(){

        return $this->belongsToMany('App\Models\Subject','teacher_subject')
                    ->withPivot('subject_id', 'teacher_id')
                    ->withTimestamps();
    }
}
